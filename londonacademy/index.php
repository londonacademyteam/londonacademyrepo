<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>London</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="css/owl.carousel.css" rel="stylesheet" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">
 <div class="container">
   <a href="#" class="logo"></a>

   </div>
   <div class="clearer"></div>
   <div class="clearer"></div>
 </div>
<div id="contentarea" class="bglogin">	
 <div class="container adminarea">
 <!--<div style="position:relative;left:900px; display:inline-table"><a href="parent/index.php" style="color:#FFFFFF; ">Parent Login</a></div>-->
  <h5>Login</h5>
  <div class="loginbox">
<?php
	if (isset($_COOKIE['username'])&&isset($_COOKIE['password'])) 
	{
		header("location:do.php");		
		//echo "ll";
	}
?>

<?php
session_start();
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 

   <h6>Please provide your details</h6>
   <form action="do.php" class="login clearfix" method="post">
         <div class="row">
           <div class="col-sm-6">  
             <div class="input-group">
                  <div class="input-group-addon"><span class="adminicon"></span></div>
                    <input type="text" name="userName" id="userName" class="form-control" placeholder="Username" value="<?php if (@isset($_COOKIE['username'])) 
	{echo $_COOKIE['username']; }?>">  
             </div>
           </div>
           <div class="col-sm-6">  
             <div class="input-group">
                  <div class="input-group-addon"><span class="passwordicon"></span></div>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
              </div>
           </div>
         </div>
         <div class="row">
           <div class="col-sm-6">  
             <div class="form-group">
               <label>
                   <input type="checkbox" name="remember" id="remember"><span class="checkboxtext"> Remember me</span>
                  </label>
                </div>
           </div>
           <div class="col-sm-6">  
             <span class="submit"><input class="loginbtn" type="submit" value="Login" name="save" id="save" ></span>
           </div>
         </div>
      </form>
   <!-- <div class="text-center password"><a href="#">Forgot your password?</a></div>  -->
  </div>
 </div>
 <div id="footer">
  <div class="container">
   <div class="powerd">
     <p>Powered By </p>
     <div class="powerdimg"><a href="http://www.bodhiinfo.com/" target="_blank"><img src="img/bodhi.png"  alt=""/></a></div>
  </div>
 </div>
</div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script> 
 
    
</body>
</html>