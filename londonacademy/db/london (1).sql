-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2016 at 01:13 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `london`
--

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`ID`, `designation`) VALUES
(1, 'Staff'),
(3, 'Teacher'),
(4, 'Clerk');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE IF NOT EXISTS `enquiry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `enquiryDate` date NOT NULL,
  `studentName` varchar(100) NOT NULL,
  `studClass` varchar(20) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `school` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `feeDetails` int(50) NOT NULL,
  `hourNeeded` int(11) NOT NULL,
  `remark` text NOT NULL,
  `parentName` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `alertDate` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `subjectId` (`subjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`ID`, `enquiryDate`, `studentName`, `studClass`, `subjectId`, `phone`, `school`, `location`, `feeDetails`, `hourNeeded`, `remark`, `parentName`, `status`, `alertDate`) VALUES
(1, '2016-01-04', 'Rahees', '12', 3, '12345', 'Jsi', 'oman', 300, 12, 'call', 'Shafeeque', 'Pending', '2016-01-05'),
(2, '2016-01-04', 'Aabith', '10', 1, '567890', 'Jamia', 'cania', 100, 2, 'msg', 'Sulaiman', 'Pending', '2016-01-08');

-- --------------------------------------------------------

--
-- Table structure for table `feepayment`
--

CREATE TABLE IF NOT EXISTS `feepayment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `paidAmount` int(11) NOT NULL,
  `paymentDate` date NOT NULL,
  `discount` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `studentId` (`studentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feepayment`
--

INSERT INTO `feepayment` (`ID`, `studentId`, `paidAmount`, `paymentDate`, `discount`) VALUES
(2, 1, 100, '2016-01-04', 100);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `staffId`, `userName`, `password`, `type`) VALUES
(1, 0, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `age` int(11) NOT NULL,
  `address` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `dateOfJoin` varchar(20) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `loginType` varchar(20) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `staffColor` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`ID`, `staffId`, `name`, `gender`, `dob`, `age`, `address`, `phone`, `email`, `dateOfJoin`, `designation`, `loginType`, `userName`, `password`, `staffColor`) VALUES
(1, '1000', 'Suneer', 'male', '02/02/1999', 17, 'alfia mansil', '987654321', 'suneer@gmail.com', '04/11/2015', '1', 'officeStaff', 'suneer', 'dfa191b5226107a218ae363d56d0d7ce', '#e13705'),
(2, '1001', 'Anees', 'male', '04/01/1989', 27, 'villa', '2345', 'anees@gmail.com', '01/12/2015', '3', 'teacher', 'anees', '4ee5d4a0a228faeadb0e3a04d070cbf0', '#a2ecc3'),
(3, '1002', 'A', 'female', '01/01/1999', 17, 'ds', 'f', 'k@gmail.com', '05/01/2016', '1', 'officeStaff', 'a', '4eae35f1b35977a00ebd8086c259d4c9', '#e06c55');

-- --------------------------------------------------------

--
-- Table structure for table `staff_subject`
--

CREATE TABLE IF NOT EXISTS `staff_subject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `subjectName` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `staff_subject`
--

INSERT INTO `staff_subject` (`ID`, `staffId`, `subjectName`) VALUES
(1, 1, 'Arabic'),
(2, 1, 'English'),
(3, 1, 'Maths'),
(4, 2, 'English'),
(5, 2, 'Maths');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `adNo` int(11) NOT NULL,
  `father` varchar(20) NOT NULL,
  `family` text NOT NULL,
  `dob` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `idcardNo` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `fatherMobile` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fatherEmail` varchar(50) NOT NULL,
  `school` varchar(50) NOT NULL,
  `classGrade` varchar(20) NOT NULL,
  `hearAbout` varchar(40) NOT NULL,
  `referalName` varchar(20) NOT NULL,
  `referalMobile` varchar(20) NOT NULL,
  `other` varchar(20) NOT NULL,
  `language` varchar(20) NOT NULL,
  `classStart` varchar(20) NOT NULL,
  `photo` text NOT NULL,
  `studColor` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ID`, `name`, `adNo`, `father`, `family`, `dob`, `gender`, `nationality`, `idcardNo`, `mobile`, `fatherMobile`, `email`, `fatherEmail`, `school`, `classGrade`, `hearAbout`, `referalName`, `referalMobile`, `other`, `language`, `classStart`, `photo`, `studColor`) VALUES
(1, 'Fathima', 1000, 'riyas', 'kammath', '01/01/1989', 'female', 'indian', '123asd', '1234567890', '234567890', 'fathima@gmail.com', 'kammath@gmail.com', 'salafia', '12', 'friends', 'tharique', '+34567890', 'haya fathima', 'english,arabic', '1 february', '', '#e13705'),
(2, 'Salafi', 1001, '', '', '04/03/2009', 'female', 'oman', 'id12345', '234567890', '', 'salafi@gmail.com', '', '', '', '', '', '', '', 'arabic', '', 'uploads/1001_ldy.jpg', '#acbfca'),
(4, 'Q', 1002, 'l', 'k', '06/01/2016', 'male', 'u', 'i', 't', 'j', 'y@gmail.com', 'y@gmail.com', '', '', 'g', 'f', 'd', 's', 'p', '', '', '#acbfca'),
(5, 'A', 1003, 'c', 'x', '06/01/2016', 'male', 'j', 'k', 'g', 's', '', '', 'b', 'v', 'w', 'e', 'r', 'f', 'm', 'n', '', '#acbfca');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance`
--

CREATE TABLE IF NOT EXISTS `student_attendance` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `remark` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `scheduleId` (`scheduleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `student_attendance`
--

INSERT INTO `student_attendance` (`ID`, `scheduleId`, `status`, `remark`) VALUES
(1, 4, 'Present', 'good');

-- --------------------------------------------------------

--
-- Table structure for table `student_schedule`
--

CREATE TABLE IF NOT EXISTS `student_schedule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `studSubjectId` int(11) NOT NULL,
  `teacherId` int(11) NOT NULL,
  `classDate` date NOT NULL,
  `timeFrom` varchar(20) NOT NULL,
  `timeTo` varchar(20) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `studentId` (`studentId`),
  KEY `subjectId` (`studSubjectId`),
  KEY `teacherId` (`teacherId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `student_schedule`
--

INSERT INTO `student_schedule` (`ID`, `studentId`, `studSubjectId`, `teacherId`, `classDate`, `timeFrom`, `timeTo`, `amount`) VALUES
(1, 1, 1, 1, '2016-01-10', '05:00:PM', '06:00:PM', 300),
(2, 1, 4, 1, '2016-01-04', '10:00:AM', '11:00:PM', 200),
(3, 2, 5, 1, '2016-01-04', '06:00:AM', '07:00:AM', 600),
(4, 2, 7, 1, '2016-01-03', '02:00:PM', '03:00:PM', 300),
(6, 1, 1, 1, '2016-01-06', '02:05:AM', '03:20:AM', 300),
(7, 2, 6, 2, '2016-01-05', '01:10:AM', '02:05:AM', 200),
(8, 2, 6, 2, '2016-01-06', '01:00:AM', '02:00:AM', 200),
(9, 1, 3, 1, '2016-01-10', '06:00:PM', '07:00:PM', 100);

-- --------------------------------------------------------

--
-- Table structure for table `student_subject`
--

CREATE TABLE IF NOT EXISTS `student_subject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `studentId` (`studentId`),
  KEY `subjectId` (`subjectId`),
  KEY `studentId_2` (`studentId`),
  KEY `subjectId_2` (`subjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `student_subject`
--

INSERT INTO `student_subject` (`ID`, `studentId`, `subjectId`) VALUES
(1, 1, 3),
(3, 1, 1),
(4, 1, 2),
(5, 2, 6),
(6, 2, 2),
(7, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(30) NOT NULL,
  `place` varchar(20) NOT NULL,
  `countType` varchar(20) NOT NULL,
  `fee` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`ID`, `subjectName`, `place`, `countType`, `fee`) VALUES
(1, 'English', 'Center', 'Group', 100),
(2, 'Maths', 'Center', 'Group', 200),
(3, 'Arabic', 'Center', 'Group', 300),
(4, 'Arabic', 'Home', 'Individual', 400),
(5, 'English', 'Center', 'Group', 500),
(6, 'Maths', 'Home', 'Individual', 600);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD CONSTRAINT `enquiry_ibfk_1` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `feepayment`
--
ALTER TABLE `feepayment`
  ADD CONSTRAINT `feepayment_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `staff_subject`
--
ALTER TABLE `staff_subject`
  ADD CONSTRAINT `staff_subject_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD CONSTRAINT `student_attendance_ibfk_1` FOREIGN KEY (`scheduleId`) REFERENCES `student_schedule` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_schedule`
--
ALTER TABLE `student_schedule`
  ADD CONSTRAINT `student_schedule_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_schedule_ibfk_2` FOREIGN KEY (`studSubjectId`) REFERENCES `student_subject` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_schedule_ibfk_3` FOREIGN KEY (`teacherId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_subject`
--
ALTER TABLE `student_subject`
  ADD CONSTRAINT `student_subject_ibfk_2` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_subject_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
