-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2016 at 01:14 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `londontest`
--

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE IF NOT EXISTS `enquiry` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `enquiryDate` date NOT NULL,
  `studentName` varchar(100) NOT NULL,
  `studClass` varchar(20) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `school` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `feeDetails` int(50) NOT NULL,
  `hourNeeded` int(11) NOT NULL,
  `remark` text NOT NULL,
  `parentName` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `alertDate` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `subjectId` (`subjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `feepayment`
--

CREATE TABLE IF NOT EXISTS `feepayment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `paidAmount` int(11) NOT NULL,
  `paymentDate` date NOT NULL,
  `discount` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `studentId` (`studentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feepayment`
--

INSERT INTO `feepayment` (`ID`, `studentId`, `paidAmount`, `paymentDate`, `discount`) VALUES
(1, 1, 100, '2016-01-04', 0),
(2, 1, 100, '2016-01-04', 100);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `staffId`, `userName`, `password`, `type`) VALUES
(1, 0, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `age` int(11) NOT NULL,
  `address` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `dateOfJoin` varchar(20) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `staffColor` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`ID`, `staffId`, `name`, `gender`, `dob`, `age`, `address`, `phone`, `email`, `dateOfJoin`, `designation`, `userName`, `password`, `staffColor`) VALUES
(1, 1000, 'Suneer Aboobacker', 'male', '15/12/1982', 33, 'Valiyaveetil House,', '30205320', 'pa.suneer@gmail.com', '01/09/2015', 'officeStaff', 'suneer', '0ae6d56a7266ffb8acec54193fa6854a', '#e13705'),
(3, 1001, 'Mohamad Omaira', 'male', '11/06/1955', 61, 'Cairo,Egypt', '30600496', 'info@londonacademyqatar.com', '25/02/2015', 'teacher', 'mohamad omaira', '0ae6d56a7266ffb8acec54193fa6854a', '#de1046'),
(4, 1002, 'Malini Unni', 'female', '10/03/1972', 44, 'No: 140/65, South Ma', '77079239', 'info@londonacademyqatar.com', '30/03/2015', 'teacher', 'malini unni', '0ae6d56a7266ffb8acec54193fa6854a', '#7f491c'),
(5, 1003, 'Subba Surendra', 'male', '12/01/1996', 20, 'Nepal', '70919349', 'info@londonacademyqatar.com', '25/08/2015', 'officeStaff', 'subba surendra', '0ae6d56a7266ffb8acec54193fa6854a', '#9f2a54'),
(6, 1004, 'Naseem Banu', 'female', '19/05/1962', 54, 'Brindavan Avanue, Ko', '30205414', 'info@londonacademyqatar.com', '06/06/2015', 'teacher', 'naseem banu', '0ae6d56a7266ffb8acec54193fa6854a', '#feb174'),
(7, 1005, 'Deepa Sajikumar', 'female', '11/09/1971', 44, '303, Radhakrishna To', '70356514', 'info@londonacademyqatar.com', '12/09/2015', 'teacher', 'Deepa Sajikumar', '0ae6d56a7266ffb8acec54193fa6854a', '#0a0954'),
(8, 1006, 'Franklin Dasan Anton', 'male', '13/02/1981', 35, '316-50/5,Indra Nagar', '70356788', 'info@londonacademyqatar.com', '07/09/2015', 'teacher', 'frnaklin dasan antony', '0ae6d56a7266ffb8acec54193fa6854a', '#18a934'),
(10, 1007, 'Abdul Nazar', 'male', '13/11/1964', 51, 'Vammadath House, Eda', '66421416', 'info@londonacademyqatar.com', '19/09/2015', 'officeStaff', 'abdul nazar', '0ae6d56a7266ffb8acec54193fa6854a', '#a1e32f'),
(11, 1008, 'Rabab Mohamad', 'female', '20/05/1988', 28, 'Egypt', '66636093', 'info@londonacademyqatar.com', '05/11/2015', 'teacher', 'rabab mohamad', '0ae6d56a7266ffb8acec54193fa6854a', '#91c0fe'),
(12, 1009, 'Bhagya Lakshmi', 'female', '02/07/1977', 39, 'No 25/13, 1st floor,', '70356338', 'info@londonacademyqatar.com', '14/11/2015', 'teacher', 'bhagya lakshmi', '0ae6d56a7266ffb8acec54193fa6854a', '#bbf7a0'),
(13, 1010, 'Ashkar', 'male', '22/05/1985', 31, 'Chalikkandy House, T', '30975690', 'info@londonacademyqatar.com', '17/11/2015', 'officeStaff', 'ashkar', '0ae6d56a7266ffb8acec54193fa6854a', '#5c658c');

-- --------------------------------------------------------

--
-- Table structure for table `staff_subject`
--

CREATE TABLE IF NOT EXISTS `staff_subject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `staffId` int(11) NOT NULL,
  `subjectName` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `staffId` (`staffId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `staff_subject`
--

INSERT INTO `staff_subject` (`ID`, `staffId`, `subjectName`) VALUES
(1, 1, 'Arabic'),
(2, 1, 'English'),
(3, 1, 'Maths'),
(4, 3, 'Arabic'),
(6, 4, 'Business Studies'),
(7, 4, 'Maths'),
(8, 4, 'Science'),
(9, 4, 'English'),
(10, 4, 'Travel & Tourism'),
(11, 4, 'Physics'),
(12, 4, 'Chemistry'),
(13, 4, 'Biology'),
(14, 3, 'English'),
(15, 3, 'Biology'),
(16, 3, 'Chemistry'),
(17, 3, 'Physics'),
(18, 3, 'Maths'),
(19, 3, 'Quran'),
(20, 3, 'Science'),
(21, 6, 'Biology'),
(22, 6, 'English'),
(23, 6, 'Science'),
(24, 7, 'Maths'),
(25, 7, 'Physics'),
(26, 7, 'Chemistry'),
(27, 7, 'English'),
(28, 8, 'Biology'),
(29, 8, 'English'),
(30, 8, 'Science'),
(34, 11, 'Arabic'),
(35, 11, 'Maths'),
(36, 11, 'Science'),
(37, 11, 'Physics'),
(38, 11, 'Chemistry'),
(39, 11, 'Biology'),
(40, 12, 'English'),
(41, 12, 'Ielts/toefl'),
(42, 12, 'Maths'),
(46, 6, 'All Subjects'),
(47, 4, 'All Subjects');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `adNo` int(11) NOT NULL,
  `father` varchar(20) NOT NULL,
  `family` text NOT NULL,
  `dob` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `idcardNo` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `fatherMobile` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fatherEmail` varchar(50) NOT NULL,
  `school` varchar(50) NOT NULL,
  `classGrade` varchar(20) NOT NULL,
  `hearAbout` varchar(40) NOT NULL,
  `referalName` varchar(20) NOT NULL,
  `referalMobile` varchar(20) NOT NULL,
  `other` varchar(20) NOT NULL,
  `language` varchar(20) NOT NULL,
  `classStart` varchar(20) NOT NULL,
  `photo` text NOT NULL,
  `studColor` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ID`, `name`, `adNo`, `father`, `family`, `dob`, `gender`, `nationality`, `idcardNo`, `mobile`, `fatherMobile`, `email`, `fatherEmail`, `school`, `classGrade`, `hearAbout`, `referalName`, `referalMobile`, `other`, `language`, `classStart`, `photo`, `studColor`) VALUES
(1, 'Fathima', 1000, 'riyas', 'kammath', '01/01/1989', 'female', 'indian', '123asd', '1234567890', '234567890', 'fathima@gmail.com', 'kammath@gmail.com', 'salafia', '12', 'friends', 'tharique', '+34567890', 'haya fathima', 'english,arabic', '1 february', 'uploads/1000_download.jpg', '#e13705'),
(2, 'Salafi', 1001, '', '', '04/03/2009', 'female', 'oman', 'id12345', '234567890', '', 'salafi@gmail.com', '', '', '', '', '', '', '', 'arabic', '', 'uploads/1001_ldy.jpg', '#18a934'),
(3, 'Reem Al Hajri @ Bani', 1002, '', '', '', 'male', '', '', '55560508', '', '', '', '', '', '', '', '', '', 'English', '', '', '#de1046'),
(4, 'Al Noud @ Al Waab', 1003, '', '', '', 'male', '', '', '556583383', '', '', '', '', '', '', '', '', '', 'English', '', '', '#7f491c'),
(5, 'Tameem @ Rayyan', 1004, '', '', '', 'male', '', '', '55576786', '', '', '', '', '', '', '', '', '', '', '', '', '#9f2a54'),
(6, 'Hanoof, Fathima, Ebs', 1005, '', '', '', 'female', '', '', '55882293', '', '', '', '', '', '', '', '', '', '', '', '', '#feb174'),
(7, 'Mariyam Nassser', 1006, 'Nasser', 'Alenazi', '', 'female', '', '', '55855538', '77755530', '', '', '', '', '', '', '', '', 'English', '', '', '#0a0954'),
(8, 'May', 1007, '', '', '', 'female', '', '', '55514833', '', '', '', '', '', '', '', '', '', '', '', '', '#bbf7a0');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance`
--

CREATE TABLE IF NOT EXISTS `student_attendance` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `scheduleId` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `remark` text NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `scheduleId` (`scheduleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `student_attendance`
--

INSERT INTO `student_attendance` (`ID`, `scheduleId`, `status`, `remark`) VALUES
(1, 4, 'Present', 'good');

-- --------------------------------------------------------

--
-- Table structure for table `student_schedule`
--

CREATE TABLE IF NOT EXISTS `student_schedule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `studSubjectId` int(11) NOT NULL,
  `teacherId` int(11) NOT NULL,
  `classDate` date NOT NULL,
  `timeFrom` varchar(20) NOT NULL,
  `timeTo` varchar(20) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `studentId` (`studentId`),
  KEY `subjectId` (`studSubjectId`),
  KEY `teacherId` (`teacherId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `student_schedule`
--

INSERT INTO `student_schedule` (`ID`, `studentId`, `studSubjectId`, `teacherId`, `classDate`, `timeFrom`, `timeTo`, `amount`) VALUES
(4, 2, 7, 1, '2016-01-03', '02:00:PM', '03:00:PM', 300),
(8, 3, 8, 11, '2016-01-08', '05:00:PM', '06:30:PM', 250),
(9, 3, 8, 11, '2016-01-07', '05:30:PM', '07:00:PM', 250),
(10, 4, 9, 12, '2016-01-04', '05:00:PM', '07:00:PM', 250),
(11, 4, 9, 12, '2016-01-05', '05:30:PM', '07:30:PM', 250),
(12, 4, 9, 12, '2016-01-06', '05:00:PM', '06:00:PM', 250),
(13, 5, 10, 12, '2016-01-04', '03:30:PM', '04:30:PM', 250),
(14, 5, 11, 12, '2016-01-06', '03:30:PM', '04:30:PM', 250),
(15, 5, 10, 12, '2016-01-11', '03:30:PM', '04:30:PM', 250),
(16, 5, 11, 12, '2016-01-13', '03:30:PM', '04:30:PM', 250),
(17, 5, 10, 12, '2016-01-18', '03:30:PM', '04:30:PM', 250),
(18, 5, 11, 12, '2016-01-20', '03:30:PM', '04:30:PM', 250),
(19, 5, 10, 12, '2016-01-25', '03:30:PM', '04:30:PM', 250),
(20, 5, 11, 12, '2016-01-27', '03:30:PM', '04:30:PM', 250),
(21, 6, 17, 6, '2016-01-04', '04:00:PM', '07:00:PM', 200),
(22, 6, 17, 6, '2016-01-05', '04:00:PM', '07:00:PM', 200),
(23, 6, 17, 6, '2016-01-06', '03:30:PM', '05:30:PM', 200),
(24, 6, 17, 6, '2016-01-11', '04:00:PM', '07:00:PM', 200),
(25, 6, 17, 6, '2016-01-12', '04:00:PM', '07:00:PM', 200),
(27, 6, 17, 6, '2016-01-13', '03:30:PM', '05:30:PM', 200),
(28, 6, 17, 6, '2016-01-18', '04:00:PM', '07:00:PM', 200),
(29, 6, 17, 6, '2016-01-19', '04:00:PM', '07:00:PM', 200),
(30, 6, 17, 6, '2016-01-20', '03:30:PM', '05:30:PM', 200),
(31, 6, 17, 6, '2016-01-25', '04:00:PM', '07:00:PM', 200),
(32, 6, 17, 6, '2016-01-26', '04:00:PM', '07:00:PM', 200),
(33, 6, 17, 6, '2016-01-27', '03:30:PM', '05:30:PM', 200),
(34, 7, 18, 4, '2015-12-23', '03:30:PM', '05:30:PM', 150),
(36, 7, 18, 4, '2015-12-27', '03:30:PM', '05:30:PM', 150),
(37, 7, 18, 4, '2015-12-30', '03:30:PM', '05:30:PM', 150);

-- --------------------------------------------------------

--
-- Table structure for table `student_subject`
--

CREATE TABLE IF NOT EXISTS `student_subject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `studentId` (`studentId`),
  KEY `subjectId` (`subjectId`),
  KEY `studentId_2` (`studentId`),
  KEY `subjectId_2` (`subjectId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `student_subject`
--

INSERT INTO `student_subject` (`ID`, `studentId`, `subjectId`) VALUES
(5, 2, 6),
(6, 2, 2),
(7, 2, 3),
(8, 3, 6),
(9, 4, 30),
(10, 5, 14),
(11, 5, 6),
(17, 6, 61),
(18, 7, 60),
(19, 8, 8);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(30) NOT NULL,
  `place` varchar(20) NOT NULL,
  `countType` varchar(20) NOT NULL,
  `fee` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`ID`, `subjectName`, `place`, `countType`, `fee`) VALUES
(1, 'English', 'Center', 'Group', 100),
(2, 'Maths', 'Home', 'Group', 200),
(3, 'Arabic', 'Center', 'Group', 100),
(6, 'Maths', 'Home', 'Individual', 250),
(7, 'Maths', 'Center', 'Group', 100),
(8, 'Maths', 'Center', 'Individual', 150),
(9, 'English', 'Center', 'Individual', 150),
(10, 'Arabic', 'Center', 'Individual', 150),
(11, 'Arabic', 'Home', 'Individual', 250),
(12, 'Arabic', 'Home', 'Group', 200),
(13, 'English', 'Home', 'Group', 200),
(14, 'English', 'Home', 'Individual', 250),
(15, 'Science', 'Center', 'Group', 100),
(16, 'Science', 'Center', 'Individual', 150),
(17, 'Science', 'Home', 'Group', 200),
(18, 'Science', 'Home', 'Individual', 250),
(19, 'Act', 'Center', 'Group', 100),
(20, 'Act', 'Center', 'Individual', 150),
(21, 'Act', 'Home', 'Group', 200),
(22, 'Act', 'Home', 'Individual', 250),
(23, 'Sat', 'Center', 'Group', 100),
(24, 'Sat', 'Center', 'Individual', 150),
(25, 'Sat', 'Home', 'Group', 200),
(26, 'Sat', 'Home', 'Individual', 250),
(27, 'Ielts/toefl', 'Center', 'Group', 100),
(28, 'Ielts/toefl', 'Center', 'Individual', 150),
(29, 'Ielts/toefl', 'Home', 'Group', 200),
(30, 'Ielts/toefl', 'Home', 'Individual', 250),
(31, 'Business Studies', 'Center', 'Group', 100),
(32, 'Business Studies', 'Center', 'Individual', 150),
(33, 'Business Studies', 'Home', 'Group', 200),
(34, 'Business Studies', 'Home', 'Individual', 250),
(35, 'Travel & Tourism', 'Center', 'Group', 100),
(36, 'Travel & Tourism', 'Center', 'Individual', 150),
(37, 'Travel & Tourism', 'Home', 'Group', 200),
(38, 'Travel & Tourism', 'Home', 'Individual', 250),
(39, 'Physics', 'Center', 'Group', 100),
(40, 'Physics', 'Center', 'Individual', 150),
(41, 'Physics', 'Home', 'Group', 200),
(42, 'Physics', 'Home', 'Individual', 250),
(43, 'Chemistry', 'Center', 'Group', 100),
(44, 'Chemistry', 'Center', 'Individual', 150),
(45, 'Chemistry', 'Home', 'Group', 200),
(46, 'Chemistry', 'Home', 'Individual', 250),
(47, 'Biology', 'Center', 'Group', 100),
(48, 'Biology', 'Center', 'Individual', 150),
(49, 'Biology', 'Home', 'Group', 200),
(50, 'Biology', 'Home', 'Individual', 250),
(51, 'Quran', 'Center', 'Group', 100),
(52, 'Quran', 'Center', 'Individual', 150),
(53, 'Quran', 'Home', 'Group', 200),
(54, 'Quran', 'Home', 'Individual', 250),
(59, 'All Subjects', 'Center', 'Group', 100),
(60, 'All Subjects', 'Center', 'Individual', 150),
(61, 'All Subjects', 'Home', 'Group', 200),
(62, 'All Subjects', 'Home', 'Individual', 250);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD CONSTRAINT `enquiry_ibfk_1` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `feepayment`
--
ALTER TABLE `feepayment`
  ADD CONSTRAINT `feepayment_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `staff_subject`
--
ALTER TABLE `staff_subject`
  ADD CONSTRAINT `staff_subject_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD CONSTRAINT `student_attendance_ibfk_1` FOREIGN KEY (`scheduleId`) REFERENCES `student_schedule` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_schedule`
--
ALTER TABLE `student_schedule`
  ADD CONSTRAINT `student_schedule_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_schedule_ibfk_2` FOREIGN KEY (`studSubjectId`) REFERENCES `student_subject` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_schedule_ibfk_3` FOREIGN KEY (`teacherId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_subject`
--
ALTER TABLE `student_subject`
  ADD CONSTRAINT `student_subject_ibfk_2` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_subject_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
