<?php 
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
$loginId= $_SESSION['LogID'];
$loginType= $_SESSION['LogType'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>London Academy</title>
<script type="text/javascript" src="../../js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery_pagination.js"></script>

	<script src="../../js/jquery-2.1.4.min.js"></script>
    <!--<script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.bxslider.js"></script>
    <script src="../../js/script.js"></script>-->

<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/newStyle.css" rel="stylesheet">
<link href="../../css/jQuery-pagination.css" rel="stylesheet">
<link href="../../font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />

<!-- timepicker -->
<link rel="stylesheet" type="text/css" href="../../css/bootstrap-datetimepicker.min.css">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">
  <div class="container"> <a href="#" class="logo"></a>
    <div class="navigation">
      <div class="dropdown"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Settings <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword/new.php">Change Password</a></li>
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>
<div id="contentarea">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu">
          <ul>
            <?php
            if($loginType=='admin')
            {
            ?>
            <li> <a href="../adminDash/admin-dash.php" class="dropdown-toggle" > <span class="menuicons icn1"></span> Home </a></li>

			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn2"></span> Registration</a>
              <ul class="dropdown-menu dropdown2" role="menu">
				<li><a href="../enquiry/new.php">Enquiry Details</a></li>
                <li><a href="../student/new.php">Student Registration</a></li>
                <li><a href="../staff/new.php">Staff Registration</a></li>
                <li><a href="../subject/new.php">Subject Registration</a></li>
                 <li><a href="../designation/new.php">designation Registration</a></li>
              </ul>
            </li>           
            
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn4"></span>Schedule</a>
              <ul class="dropdown-menu dropdown2" role="menu">
              	<li><a href="../studentSchedule/new.php">Individual</a></li>
			  	<li><a href="../studentScheduleGp/new.php">Group</a></li>			                      
			  </ul>
           </li> 
            
           <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn4"></span>Calendar</a>
              <ul class="dropdown-menu dropdown2" role="menu">
              	<li><a href="../studentSchedule/studentCalendar.php">Student</a></li>
			  	<li><a href="../staffSchedule/staffCalendar.php">Teacher</a></li>                             
			  </ul>
           </li>          
           <li><a href="../studentAttendance/new.php" class="dropdown-toggle"> <span class="menuicons icn5"></span>Attendance</a></li>
           <li> <a href="../feePayment/new.php" class="dropdown-toggle" > <span class="menuicons icn6"></span>Fee Payment</a></li>
           <li><a href="../diary/student.php" class="dropdown-toggle"> <span class="menuicons icn10"></span>Diary</a></li>
           <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn8"></span>Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			  	<li><a href="../report/studentSchedule.php">Student Schedule</a></li>
                <li><a href="../report/teacherSchedule.php">Teacher Schedule</a></li> 
                <li><a href="../report/studAtt.php">Student Attendance</a></li> 
                <li><a href="../report/feeReport.php">Fee Payment</a></li>   
                <li><a href="../report/student.php">Student</a></li> 
                <li><a href="../report/staff.php">Staff</a></li>       
			  </ul>
           </li>
          <li> <a href="../student/delete.php" class="dropdown-toggle" > <span class="menuicons icn11"></span> Delete student </a></li>
           <?php  
			}
            
            else if($loginType=='officeStaff')
            {
            ?>
             <li> <a href="../adminDash/admin-dash.php" class="dropdown-toggle" > <span class="menuicons icn1"></span> Home </a></li>

			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn2"></span> Registration</a>
              <ul class="dropdown-menu dropdown2" role="menu">
				<li><a href="../enquiry/new.php">Enquiry Details</a></li>
                <li><a href="../student/new.php">Student Registration</a></li>               
                <li><a href="../subject/new.php">Subject Registration</a></li>
                 <li><a href="../designation/new.php">designation Registration</a></li>
              </ul>
            </li>           
            
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn4"></span>Schedule</a>
              <ul class="dropdown-menu dropdown2" role="menu">
              	<li><a href="../studentSchedule/new.php">Individual</a></li>
			  	<li><a href="../studentScheduleGp/new.php">Group</a></li>			                      
			  </ul>
           </li> 
            
           <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn4"></span>Calendar</a>
              <ul class="dropdown-menu dropdown2" role="menu">
              	<li><a href="../studentSchedule/studentCalendar.php">Student</a></li>
			  	<li><a href="../staffSchedule/staffCalendar.php">Teacher</a></li>                             
			  </ul>
           </li>          
           <li><a href="../studentAttendance/new.php" class="dropdown-toggle"> <span class="menuicons icn5"></span>Attendance</a></li>
           <li> <a href="../feePayment/new.php" class="dropdown-toggle" > <span class="menuicons icn6"></span>Fee Payment</a></li>
           <li><a href="../diary/student.php" class="dropdown-toggle"> <span class="menuicons icn10"></span>Diary</a></li>
           <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn8"></span>Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			  	<li><a href="../report/studentSchedule.php">Student Schedule</a></li>
                <li><a href="../report/teacherSchedule.php">Teacher Schedule</a></li> 
                <li><a href="../report/studAtt.php">Student Attendance</a></li> 
                <li><a href="../report/feeReport.php">Fee Payment</a></li>   
                <li><a href="../report/student.php">Student</a></li> 
                <li><a href="../report/staff.php">Staff</a></li>       
			  </ul>
           </li>
           <?php  
			}
			 
            ?>
                               		 
		 </ul>
        </div>
      </div>