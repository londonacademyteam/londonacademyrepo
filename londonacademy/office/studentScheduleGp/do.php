<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['groupName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=$s=0;
				
				$groupName				=	ucwords(strtolower($App->convert($_REQUEST['groupName'])));
				$data['groupName']		=	$groupName;
				$subjectId				=	$App->convert($_REQUEST['subjectId']);
				$data['subjectId']		=	$subjectId;
				$existId				=	$db->existValuesId(TABLE_STUDENT_GROUP,"groupName='$groupName' and subjectId='$subjectId'");
				if($existId>0)
				{
					$_SESSION['msg']="Group Already Exist";													
				}
				else
				{	
					$success=$db->query_insert(TABLE_STUDENT_GROUP,$data);				
					for($k=1;$k<=$_POST['leng'];$k++)
					{	
					   $studentId			=	'';									 
					   $studentId			=	"studentId".$k;					   
					   @$studentId			=	$App->convert($_REQUEST[$studentId]);
					   
					   
					   $existId2				=	$db->existValuesId(TABLE_GROUP_DETAILS,"groupId='$success' and studentId='$studentId'");
					   if($studentId=='')  					   					   					
					   	{
							continue;
						}					
						elseif($existId2>0)
						{
							continue;													
						}
						else{
							$data2['groupId']	=	$success;	
							$data2['studentId']	=	$studentId;	
				   			$s=$db->query_insert(TABLE_GROUP_DETAILS,$data2);	
						}   								
					}
				}											
				$db->close();
				if($s)
				{					
				$_SESSION['msg']="Group Created Successfully";					
				
				}	
				header("location:new.php");					
			}		
		break;		
	case 'group':
		
		if(!$_REQUEST['subjectId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
								
				$teacherId	=	$App->convert($_REQUEST['teacherId']);
				$subjectId	=	$App->convert($_REQUEST['subjectId']);
				$amount		=	$App->convert($_REQUEST['amount']);
				$classDate	=	$App->dbformat_date($_REQUEST['classDate']);
				$timeFrom	=	$_REQUEST['timeFromHr'].":".$_REQUEST['timeFromMin']." ".$_REQUEST['type1'];
				$timeTo		=	$_REQUEST['timeToHr'].":".$_REQUEST['timeToMin']." ".$_REQUEST['type2'];
				//$timeFrom	=	$App->convert($_REQUEST['timeFrom']);
				//$timeTo		=	$App->convert($_REQUEST['timeTo']);
				
				$timeFrom  = date("H:i:s", strtotime($timeFrom));
				$timeTo    = date("H:i:s", strtotime($timeTo));
				//echo $classDate.$timeFrom.$timeTo.$subjectId.$amount.$teacherId;die;				
				
				//********
		
				$k=1;$j=0;$flag1=$flag2=0;	
				 	//echo $_POST['leng'];die;		   
					for($k=1;$k<=$_POST['leng'];$k++)
					{							
					   $studentId			=	'';									 
					   $student				=	"studentId".$k;					   
					   @$studentId			=	$App->convert($_REQUEST[$student]);
					  
					   if($studentId=='')  					   					   					
					   	{
							continue;
						}											
						else
						{						  				
							$existId2=$db->existValuesId(TABLE_STUDENT_SCHEDULE,"teacherId='$teacherId' 
																	AND classDate='$classDate' 
																	AND subjectId!=$subjectId
																	AND ((timeFrom='$timeFrom' && timeTo='$timeTo') 
																		OR (timeFrom>'$timeFrom' && timeTo>'$timeTo' && timeFrom<'$timeTo') 
																		OR (timeFrom<'$timeFrom' && timeTo>'$timeFrom' && timeTo<'$timeTo')
																		OR (timeFrom>'$timeFrom' && timeTo<'$timeTo') 
																		OR (timeFrom<'$timeFrom' && timeTo>'$timeTo'))");							
																						
							if($existId2>0)
							{
							$_SESSION['msg']="Teacher already scheduled for another subject";					
							header("location:new.php");					
							}
							$existId=$db->existValuesId(TABLE_STUDENT_SCHEDULE,"studentId='$studentId' 
														AND classDate='$classDate' 
														AND ((timeFrom='$timeFrom' && timeTo='$timeTo') 
															OR (timeFrom>'$timeFrom' && timeTo>'$timeTo' && timeFrom<'$timeTo') 
															OR (timeFrom<'$timeFrom' && timeTo>'$timeFrom' && timeTo<'$timeTo')
															OR (timeFrom>'$timeFrom' && timeTo<'$timeTo') 
															OR (timeFrom<'$timeFrom' && timeTo>'$timeTo'))");
							//echo $existId;die;														
				
							if($existId>0)
							{							
								$exitStudent[$j]=$studentId;
								$flag1=1;	
								//echo $flag1;die;						
								$j++;																						
							}
					
							else
							{	
													
							$data['studentId']		=	$studentId;							
							$data['subjectId']		=	$subjectId;
							$data['classDate']		=	$classDate;
							$data['timeFrom']		=	$timeFrom;
							$data['timeTo']			=	$timeTo;
							$data['teacherId']		=	$teacherId;				
							$data['amount']			=	$amount;
								
							$success=$db->query_insert(TABLE_STUDENT_SCHEDULE,$data);
							$flag2=1;	
							}
							//else
						}//else												
					}	//for						
				
					if($flag1==0 && $flag2==0)
					{
						$_SESSION['msg']="Scheduling failed";										
					}	
					else if($flag1==0 && $flag2==1)
					{
						$_SESSION['msg']="Students schedulled successfully";										
					}
					else if($flag1==1 && $flag2==0)
					{
						$_SESSION['msg']="Students already Scheduled";										
					}
					else if($flag1==1 && $flag2==1)
					{
						
						$_SESSION['msg']="Students schedulled successfully.Scheduling already exist for";
						for($p=0;$p<$j;$p++)
						{							
							$studId =	$exitStudent[$p];
							$select	=	"select name from ".TABLE_STUDENT." where ID='$studId'";							
							$res	=	mysql_query($select);
							$row	=	mysql_fetch_array($res);
							$name	=	$row['name'];
							$_SESSION['names']=$_SESSION['names'].$name."/";						
						}
																
					}
					$db->close();	
				header("location:new.php");
		}				
														
		break;		
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
					@mysql_query("DELETE FROM `".TABLE_GROUP_DETAILS."` WHERE groupId='{$deleteId}'");
					$success=@mysql_query("DELETE FROM `".TABLE_STUDENT_GROUP."` WHERE ID='{$deleteId}'");				      				
								            													
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Group deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");					
		break;		
		
}
?>