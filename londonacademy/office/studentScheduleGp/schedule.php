<?php include("../adminHeader.php"); ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$groupId	=	$_REQUEST['gpId'];

?>

<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

function clearStaff()
{
document.getElementById('teacherId').value="";
document.getElementById('teacherScheduleDiv').innerHTML='';																					
}
function getTeacherSchedule()
{
	flag=0;
	classDate	=	document.getElementById('classDate').value;
	
	jTimeFromHr	=	document.getElementById('timeFromHr').value;
	jTimeFromMin=	document.getElementById('timeFromMin').value;
	jType1		=	document.getElementById('type1').value;
	
	jTimeToHr	=	document.getElementById('timeToHr').value;
	jTimeToMin	=	document.getElementById('timeToMin').value;	
	jType2		=	document.getElementById('type2').value;
	
	
	timeFrom	=	jTimeFromHr+":"+jTimeFromMin+" "+jType1;
	timeTo		=	jTimeToHr+":"+jTimeToMin+" "+jType2;
	teacherId	=	document.getElementById('teacherId').value;
	
		if(classDate=='')
		{		
		document.getElementById('classDateDiv').innerHTML="Can't leave this empty";	
		document.getElementById('teacherId').value="";
		flag=1;		
		}
		
	if(flag==0)
	{
		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    document.getElementById("teacherScheduleDiv").innerHTML=xmlhttp.responseText;
		    }
		  }
		  xmlhttp.open("GET","getTeacherSchedule.php?classDate="+classDate+"&timeFrom="+timeFrom+"&timeTo="+timeTo+"&teacherId="+teacherId,true);
		
		xmlhttp.send();
	}
}
function getFee()
{	
	subjectId	=	document.getElementById('subjectId').value;
	jTimeFromHr	=	document.getElementById('timeFromHr').value;
	jTimeFromMin=	document.getElementById('timeFromMin').value;
	jType1		=	document.getElementById('type1').value;
	
	jTimeToHr	=	document.getElementById('timeToHr').value;
	jTimeToMin	=	document.getElementById('timeToMin').value;	
	jType2		=	document.getElementById('type2').value;
	
	
	timeFrom	=	jTimeFromHr+":"+jTimeFromMin+" "+jType1;
	timeTo		=	jTimeToHr+":"+jTimeToMin+" "+jType2;
		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    document.getElementById('amount').value=xmlhttp.responseText;
		    }
		  }
		  xmlhttp.open("GET","getFeeForGroup.php?subjectId="+subjectId+"&timeFrom="+timeFrom+"&timeTo="+timeTo,true);		
		xmlhttp.send();
}
</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">GROUP SCHEDULE</h2> 					
				</div>
          </div> 
                 
        </div>
    
      <!--scheduling-->
        <form action="do.php?op=group"  class="form1" method="post" onsubmit="return valid()">        
         <div class="tablearea table-responsive">
              <table class="table">                              
                <tbody>
				<tr> 				 						            								
	               
	             <td>
					<label for="classDate">ClassDate: </label>
					<input type="text" name="classDate" id="classDate" class="form-control2 datepicker" required  width="5%" placeholder="DD/MM/YYYY" readonly="readonly" onfocus="clearbox('classDateDiv')"/>	
		  			<div id="classDateDiv" class="valid" style="color:#FF6600;"></div> 
	  			</td>
				 <td>
					<table>				
						<tr>				
							<td style="padding-right:5px;">
							<label for="timeFromHr">From Time: </label><br>	
								<select name="timeFromHr" id="timeFromHr" required style="height:34px;" onchange="clearStaff()" onfocus="clearbox('fromDiv')">
									<option value="">HH</option>
									<?php
									for($i=1;$i<=12;$i++)
									{
									?>
									<option value="<?php if($i<10){ echo "0".$i;}else{echo $i;} ?>"><?php if($i<10){ echo "0".$i;}else{echo $i;} ?></option>
									<?php	
									}
									?>
								</select>
							
							<label ></label>	
								<select name="timeFromMin" id="timeFromMin"  required style="height:34px;" onchange="clearStaff()" onfocus="clearbox('fromDiv')">
									<option value="">MM</option>
									<option value="00">00</option>
									<option value="30">30</option>
									
									<?php
									/*for($i=0;$i<60;$i++)
									{
										if($i%5==0)
										{						
									?>
									<option value="<?php if($i<10){ echo "0".$i;}else{echo $i;} ?>"><?php if($i<10){ echo "0".$i;}else{echo $i;} ?></option>
									<?php
										}	
									}*/
									?>
								</select>
								
							
							<label ></label>
								<select name="type1" id="type1" style="height:34px;" onchange="clearStaff();" onfocus="clearbox('fromDiv')">					
			                    	<option value="AM">AM</option>
									<option value="PM">PM</option>						
			                    </select>
							</td>																																																			
						</tr>
				</table>
				<div id="fromDiv" class="valid" style="color:#FF6600;"></div> 
			</td>
			<td>
				<table>								
					<tr>				
						<td style="padding-right: 5px;">
						<label for="timeToHr">To Time: </label>	<br>
							<select name="timeToHr" id="timeToHr" required style="height:34px;" onchange="clearStaff()"  onfocus="clearbox('toDiv')">
								<option value="">HH</option>
								<?php
								for($i=1;$i<=12;$i++)
								{
								?>
								<option value="<?php if($i<10){ echo "0".$i;}else{echo $i;} ?>"><?php if($i<10){ echo "0".$i;}else{echo $i;} ?></option>
								<?php	
								}
								?>
							</select>
						
						<label for="timeToMin"></label>	
							<select name="timeToMin" id="timeToMin"  required style="height:34px;" onchange="clearStaff()"   onfocus="clearbox('toDiv')">
								<option value="">MM</option>
								<option value="00">00</option>
								<option value="30">30</option>
								<?php
								/*for($i=0;$i<60;$i++)
								{
									if($i%5==0)
									{						
								?>
								<option value="<?php if($i<10){ echo "0".$i;}else{echo $i;} ?>"><?php if($i<10){ echo "0".$i;}else{echo $i;} ?></option>						
								<?php
									}	
								}*/
								?>
							</select>
						
						<label ></label>
							<select name="type2" id="type2" style="height:34px;" onchange="clearStaff()"   onfocus="clearbox('toDiv')">
		                    	<option value="AM">AM</option>
								<option value="PM">PM</option>						
		                    </select>
						</td>																											
					</tr>						
				</table>
				<div id="toDiv" class="valid" style="color:#FF6600;"></div>
			</td>
		</tr>
	    <tr>		
				       			
			<td>	
			<label for="subjectId">Subject: </label>		                                     
	          
                   
                    <?php								
					$select3=mysql_query("select ".TABLE_SUBJECT.".ID,
													".TABLE_SUBJECT.".subjectName,
													 ".TABLE_SUBJECT.".place,
													 ".TABLE_SUBJECT.".countType,
													  ".TABLE_SUBJECT.".fee														
												from ".TABLE_SUBJECT.",".TABLE_STUDENT_GROUP."	
													where ".TABLE_SUBJECT.".ID =".TABLE_STUDENT_GROUP.".subjectId
														and ".TABLE_STUDENT_GROUP.".ID='$groupId'");
														
					  $subRow=mysql_fetch_array($select3);						 
					?>
                  	<input type="text" name="subject" id="subject" required="required" readonly="readonly" class="form-control2" width="15%" value="<?php echo $subRow['subjectName']."-".$subRow['place']."-".$subRow['countType'];?>" >
                  	<input type="hidden" name="subjectId" id="subjectId" value="<?php echo $subRow['ID'] ?>">

	             </td>
	             
			<td>
				<label for="teacherId">Teacher: </label>					                    
               <select name="teacherId" id="teacherId"  class="form-control2" required onchange="getTeacherSchedule();getFee();">
                <option value="">Select</option>			                
			    <?php
			    	$subjectName=$subRow['subjectName'];
			    	$sel="select ".TABLE_STAFF.".name,
  							 ".TABLE_STAFF.".ID 
  						from ".TABLE_STAFF.",
  							 ".TABLE_STAFF_SUBJECT."
  					   where ".TABLE_STAFF_SUBJECT.".subjectName='$subjectName'  						  						 
  						   and ".TABLE_STAFF_SUBJECT.".staffId=".TABLE_STAFF.".ID
  						  group by  ".TABLE_STAFF.".name
  						order by  ".TABLE_STAFF.".name";											
					$res=mysql_query($sel);
					while($row=mysql_fetch_array($res))
					{
				    ?>
					   <option value="<?php echo $row['ID']; ?>"><?php echo $row['name']; ?></option>
				  <?php }?>

	                  </select>
				<div id="teacherScheduleDiv"></div>   					             
			</td>
			<td>
				 <label for="amount">Amount: </label>
				 <input type="text" name="amount" id="amount"  class="form-control2">
				 <div id="amountDiv" class="valid" style="color:#FF6600;"></div>		                   
	        </td>
			
			
		</tr>
       </tbody>
      </table>
      
    </div>
    <div class="tablearea3 table-responsive">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
                <table class="table  view_limitter pagination_table" >
                  <thead>
                    <tr>
                      <td>SlNo.</td>                      
                      <td>Student Name</td> 
                      <td>Status</td>                                      
                    </tr>
                  </thead>
                  <tbody>
				<?php 															
						$selAllQuery5="select `".TABLE_STUDENT."`.name,
											 `".TABLE_STUDENT."`.ID																						 											  
										from ".TABLE_GROUP_DETAILS.",`".TABLE_STUDENT."`
									   where `".TABLE_GROUP_DETAILS."`.groupId='$groupId' 									   									  
									   	and	 `".TABLE_GROUP_DETAILS."`.studentId=`".TABLE_STUDENT."`.ID
								   order by `".TABLE_STUDENT."`.name ";
										
						$selectAll5= $db->query($selAllQuery5);
						$number5=mysql_num_rows($selectAll5);					
						if($number5==0)
						{
						?>
                         <tr>
                            <td align="center" colspan="3">
                                There is no data in list.
                            </td>
                        </tr>
						<?php
						}
						else
						{
							$i=0;
							while($row5=mysql_fetch_array($selectAll5))
							{	
							$tableId=$row5['ID'];							
							?>
							  <tr>
		                       	<td><?php echo ++$i;?></td>
		                       	<td><?php echo $row5['name']; ?></td> 	
		                       	<td><input type="checkbox" name="studentId<?php echo $i;?>" id="studentId<?php echo $i;?>" value="<?php echo $tableId ?>" checked="checked" ></td>                                								
	                       	
							  </tr>
					  <?php }
						}?>                  
                </tbody>
                </table>
              </div>
              <!-- paging -->		
            <div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div>        
            <!-- paging end-->
            </div>
            <input type="hidden" name="leng" value="<?php echo $i;?>">
    </form>
       <!--scheduling end-->
      
        
     
      </div>
      
      
   
<?php include("../adminFooter.php") ?>
