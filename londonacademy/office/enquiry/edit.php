<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_ENQUIRY."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>
<script>
//get fee
 function getFee(str)
 {	
		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    document.getElementById("feeDetails").value=xmlhttp.responseText;
		    }
		  }
		  xmlhttp.open("GET","getFeeAjax.php?subject="+str,true);
		
		xmlhttp.send();
 }
</script>
 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">ENQUIRY DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
               <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="enquiryDate">Enquiry Date:<span class="valid">*</span></label>
                      <input type="text" name="enquiryDate" id="enquiryDate" class="form-control2 datepicker" required value="<?php echo $App->dbFormat_date($editRow['enquiryDate']) ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="studentName">Student Name:<span class="valid">*</span></label>
                        <input type="text" class="form-control2" name="studentName" id="studentName" required value="<?php echo $editRow['studentName']; ?>">	
                    </div>
                    <div class="form-group">
                      <label for="studClass">Class:</label>
                      <input type="text" class="form-control2" name="studClass" id="studClass" value="<?php echo $editRow['studClass']; ?>" >
                    </div>
                    <div class="form-group">
                      <label for="subjectId">Subject:<span class="valid">*</span></label>
                      <select name="subjectId" id="subjectId" class="form-control2" required  onchange="getFee(this.value)">
							<option value="">Select</option>
							<?php 
							$select2="select * from ".TABLE_SUBJECT." order by subjectName";
							$res2=mysql_query($select2);
							while($row2=mysql_fetch_array($res2))
							{
							?>
							<option value="<?php echo $row2['ID']?>" <?php if($editRow['subjectId']==$row2['ID']){?> selected<?php }?>><?php echo $row2['subjectName']." - ".$row2['place']." - ".$row2['countType'];?></option>
							<?php									
							}
							?>				
						</select>
                    </div>
                    <div class="form-group">
                      <label for="feeDetails">Fee Details:</label>
                      <input type="text" class="form-control2" name="feeDetails" id="feeDetails" value="<?php echo $editRow['feeDetails']; ?>" >					 
                    </div>
                    <div class="form-group">
                      <label for="hourNeeded">Hour Needed:</label>
                      <input type="text" class="form-control2" name="hourNeeded" id="hourNeeded"  value="<?php echo $editRow['hourNeeded']; ?>">					 
                    </div>
                   
                    <div class="form-group">
                      <label for="phone">Phone:<span class="valid">*</span></label>
                      <input type="text" name="phone" id="phone" class="form-control2" value="<?php echo $editRow['phone']; ?>">	
                    </div>                		
                             					
                    <div class="form-group">
                      <label for="School">School:</label>
                      <input type="text" name="school" id="school" class="form-control2" value="<?php echo $editRow['school']; ?>">
                    </div>
				</div>
                 <div class="col-sm-6">		
                    <div class="form-group">
                      <label for="location">Location:</label>
                      <input type="text" name="location" id="location" class="form-control2" value="<?php echo $editRow['location']; ?>">
                    </div>
                 
                    <div class="form-group">
                      <label for="building">Building:</label>
                      <input type="text" name="building" id="building" class="form-control2"  value="<?php echo $editRow['building'];  ?>" >			  
                    </div>
                    <div class="form-group">
                      <label for="street">Street:</label>
                      <input type="text" name="street" id="street" class="form-control2"  value="<?php echo $editRow['street'];  ?>" >			  
                    </div>
                    <div class="form-group">
                      <label for="zone">Zone:</label>
                      <input type="text" name="zone" id="zone" class="form-control2"  value="<?php echo $editRow['zone'];  ?>" >			  
                    </div>								 
                  	<div class="form-group">
                      <label for="parentName">Parent Name:</label>
                      <input type="text" name="parentName" id="parentName" class="form-control2" value="<?php echo $editRow['parentName']; ?>">
                    </div>							 
                  				
                    <div class="form-groupy">
                      <label for="remark">Remark:</label>
                      <textarea id="remark" name="remark" class="form-control2"><?php echo $editRow['remark']; ?></textarea>			  
                    </div>
                    <div class="form-group">
                      <label for="alertDate">Alert Date:<span class="valid">*</span></label>
                      <input type="text" name="alertDate" id="alertDate" class="form-control2 datepicker" required value="<?php echo  $App->dbFormat_date($editRow['alertDate']) ?>" readonly>
                    </div>
                    
                    <div class="form-group">
                      <label for="status">Status:</label>
                      <select name="status" id="status" class="form-control2" >
                      	<option value="Pending" <?php if($editRow['status']=='Pending'){ echo 'selected';} ?>>Pending</option>
                        <option value="Joined" <?php if($editRow['status']=='Joined'){ echo 'selected';} ?>>Joined</option>
                        <option value="Cancelled" <?php if($editRow['status']=='Cancelled'){ echo 'selected';} ?>>Cancelled</option>
                        <option value="Waiting" <?php if($editRow['status']=='Waiting'){ echo 'selected';} ?>>Waiting</option>
                      </select>
                    </div>						
                  </div> 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
