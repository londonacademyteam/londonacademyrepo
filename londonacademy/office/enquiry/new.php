<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

//get fee
 function getFee(str)
 {	
		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    document.getElementById("feeDetails").value=xmlhttp.responseText;
		    }
		  }
		  xmlhttp.open("GET","getFeeAjax.php?subject="+str,true);
		
		xmlhttp.send();
 }
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">ENQUIRY DETAILS</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4" >
            <form method="post">
              <div class="input-group">
                <input type="text" class="form-control"  name="name" placeholder="Student Name / Phone / Subject / Status" value="<?php echo @$_REQUEST['name'] ?>">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
		 <?php	
			$cond="1";
			if(@$_REQUEST['name'])
			{			
				$cond=$cond." and ((".TABLE_ENQUIRY.".studentName like'%".$_POST['name']."%') or (".TABLE_ENQUIRY.".phone like'%".$_POST['name']."%') or (".TABLE_SUBJECT.".subjectName like'%".$_POST['name']."%') or (".TABLE_ENQUIRY.".status like'%".$_POST['name']."%'))";
			}
			
			?>
			<div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr>
                  	<th>SlNo</th>                    
                    <th>Student</th>					
					<th>Class</th>
					<th>Subject</th>
					<th>Phone</th>
                    <th>Status</th>
                    <th>Enquiry Date</th>
					<th></th>
                    <th></th>
            
													
                  </tr>
                </thead>
                <tbody>
						<?php 
						$i=1;
						$selectAll=mysql_query("select ".TABLE_ENQUIRY.".ID,
														".TABLE_ENQUIRY.".enquiryDate,
														".TABLE_ENQUIRY.".studentName,
														".TABLE_ENQUIRY.".studClass,
														".TABLE_SUBJECT.".subjectName,
														".TABLE_ENQUIRY.".phone,
														".TABLE_ENQUIRY.".school,
														".TABLE_ENQUIRY.".location,
														".TABLE_ENQUIRY.".building,
														".TABLE_ENQUIRY.".street,
														".TABLE_ENQUIRY.".zone,
														".TABLE_ENQUIRY.".feeDetails,
														".TABLE_ENQUIRY.".hourNeeded,
														".TABLE_ENQUIRY.".remark,
														".TABLE_ENQUIRY.".parentName,
														".TABLE_ENQUIRY.".status,
														".TABLE_ENQUIRY.".alertDate
													from `".TABLE_ENQUIRY."`,".TABLE_SUBJECT."
													where ".TABLE_SUBJECT.".ID=".TABLE_ENQUIRY.".subjectId 													
													and $cond 
													order by ".TABLE_ENQUIRY.".ID desc");					
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="9">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{							
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
                      	<td><?php echo $i; $i++; ?>
                        <div class="adno-dtls"><a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a></div>
                        </td>                       
						<td><?php echo $row['studentName'];?></td>																
						<td><?php echo $row['studClass']; ?></td>
						<td><?php echo $row['subjectName']; ?></td>
						<td><?php echo $row['phone']; ?></td>
                        <td><?php echo $row['status']; ?></td>
                        <td><?php echo $App->dbFormat_date($row['enquiryDate']); ?></td>
						<td><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>" class="viewbtn">View</a>
						<!-- Modal3 -->
						  <div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							  <div class="modal-content"> 										
								<div role="tabpanel" class="tabarea2"> 								  
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"> <a href="#details<?php echo $tableId; ?>" aria-controls="details" role="tab" data-toggle="tab">Details</a> </li>
								  </ul>								  
								  <!-- Tab panes -->
								  <div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="details<?php echo $tableId; ?>">
									  <table class="table nobg" id="headerTable2<?php echo $i; ?>" >
										<tbody style="background-color:#FFFFFF">
										  <tr>
										  	<td>Enquiry Date</td>
											<td>:</td>
											<td><?php echo $App->dbFormat_date($row['enquiryDate']); ?></td>
										  </tr>
										  <tr>
											<td>Student Name</td>
											<td>:</td>
											<td><?php echo $row['studentName']; ?></td>
										  </tr>
										  <tr>
											<td>Class</td>
											<td>:</td>
											<td><?php echo $row['studClass']; ?></td>
										  </tr>
										  <tr>
											<td>Subject</td>
											<td>:</td>
											<td><?php echo $row['subjectName']; ?></td>
										  </tr>
										  
										  <tr>
											<td>Land Phone</td>
											<td>:</td>
											<td><?php echo $row['phone']; ?></td>
										  </tr>
										  <tr>
											<td>School</td>
											<td>:</td>
											<td><?php echo $row['school']; ?></td>
										  </tr>
										  <tr>
											<td>Location</td>
											<td>:</td>
											<td><?php echo $row['location']; ?></td>
										  </tr>
										  <tr>
											<td>Building</td>
											<td>:</td>
											<td><?php echo $row['building']; ?></td>
										  </tr>
										  <tr>
											<td>Street</td>
											<td>:</td>
											<td><?php echo $row['street']; ?></td>
										  </tr>
										  <tr>
											<td>Zone</td>
											<td>:</td>
											<td><?php echo $row['zone']; ?></td>
										  </tr>
										  <tr>
											<td>Fee</td>
											<td>:</td>
											<td><?php echo $row['feeDetails']; ?></td>
										  </tr>
										  <tr>
											<td>Hour Needed</td>
											<td>:</td>
											<td><?php echo $row['hourNeeded']; ?></td>
										  </tr>
										  <tr>
											<td>Remark</td>
											<td>:</td>
											<td><?php echo $row['remark']; ?></td>
										  </tr>
										  <tr>
											<td>Parent Name</td>
											<td>:</td>
											<td><?php echo $row['parentName']; ?></td>
										  </tr>
										  <tr>
											<td>Status</td>

											<td>:</td>
											<td><?php echo $row['status']; ?></td>
										  </tr>
										  <tr>
											<td>Alert Date</td>
											<td>:</td>
											<td><?php echo $App->dbFormat_date($row['alertDate']); ?></td>
										  </tr>
										  
										</tbody>
									  </table>
									</div>
																		
								  </div>
								</div>
							  </div>
							</div>
						  </div>
						  <!-- Modal3 cls --> 						
						</td>
                        <td><?php if($row['status']!='Joined'){?><a href="../student/join.php?id=<?php echo $tableId?>"  class="viewbtn">Join</a><?php }?></td>
					  </tr>
					  <?php }
					  }
					  ?>                  
                </tbody>
              </table>
            </div>
             <!-- paging -->		
            <div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div>        
            <!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">ENQUIRY DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="enquiryDate">Enquiry Date:<span class="valid">*</span></label>
                      <input type="text" name="enquiryDate" id="enquiryDate" class="form-control2 datepicker" required value="<?php echo date('d/m/Y') ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="studentName">Student Name:<span class="valid">*</span></label>
                        <input type="text" class="form-control2" name="studentName" id="studentName" required>	
                    </div>
                    <div class="form-group">
                      <label for="studClass">Class:</label>
                      <input type="text" class="form-control2" name="studClass" id="studClass" >
                    </div>
                    <div class="form-group">
                      <label for="subjectId">Subject:<span class="valid">*</span></label>
                      <select name="subjectId" id="subjectId" class="form-control2" required  onchange="getFee(this.value)">
							<option value="">Select</option>
							<?php 
							$select2="select * from ".TABLE_SUBJECT." order by subjectName";
							$res2=mysql_query($select2);
							while($row2=mysql_fetch_array($res2))
							{
							?>
							<option value="<?php echo $row2['ID']?>"><?php echo $row2['subjectName']." - ".$row2['place']." - ".$row2['countType'];?></option>
							<?php									
							}
							?>				
						</select>
                    </div>
                    <div class="form-group">
                      <label for="feeDetails">Fee Details:</label>
                      <input type="text" class="form-control2" name="feeDetails" id="feeDetails" >					 
                    </div>
                     <div class="form-group">
                      <label for="hourNeeded">Hour Needed:</label>
                      <input type="text" class="form-control2" name="hourNeeded" id="hourNeeded" >					 
                    </div>
                  
                    <div class="form-group">
                      <label for="phone">Phone:<span class="valid">*</span></label>
                      <input type="text" name="phone" id="phone" class="form-control2" >	
                    </div>					
                              					
                    <div class="form-group">
                      <label for="School">School:</label>
                      <input type="text" name="school" id="school" class="form-control2">
                    </div>
				</div>
                 <div class="col-sm-6">	    
                    <div class="form-group">
                      <label for="location">Location:</label>
                      <input type="text" name="location" id="location" class="form-control2">
                    </div>
                    <div class="form-group">
                      <label for="building">Building:</label>
                      <input type="text" name="building" id="building" class="form-control2" >			  
                    </div>
                    <div class="form-group">
                      <label for="street">Street:</label>
                      <input type="text" name="street" id="street" class="form-control2" >			  
                    </div>
                    <div class="form-group">
                      <label for="zone">Zone:</label>
                      <input type="text" name="zone" id="zone" class="form-control2" >			  
                    </div>								 
                  	<div class="form-group">
                      <label for="parentName">Parent Name:</label>
                      <input type="text" name="parentName" id="parentName" class="form-control2">
                    </div>							 
                  				
                    <div class="form-groupy">
                      <label for="remark">Remark:</label>
                      <textarea id="remark" name="remark" class="form-control2"></textarea>			  
                    </div>
                    <div class="form-group">
                      <label for="alertDate">Alert Date:<span class="valid">*</span></label>
                      <input type="text" name="alertDate" id="alertDate" class="form-control2 datepicker" required value="<?php echo date('d/m/Y') ?>" readonly>
                    </div>					
                   </div> 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
