<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['studentName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
							
					$data['enquiryDate']	=	$App->dbFormat_date($_REQUEST['enquiryDate']);
					$data['studentName']	=	ucwords(strtolower($App->convert($_REQUEST['studentName'])));
					$data['studClass']		=	ucwords(strtolower($App->convert($_REQUEST['studClass'])));
					$data['subjectId']		=	$App->convert($_REQUEST['subjectId']);
					$data['phone']			=	$App->convert($_REQUEST['phone']);
					$data['school']			=	ucwords(strtolower($App->convert($_REQUEST['school'])));
					$data['location']		=	$App->convert($_REQUEST['location']);
					$data['building']		=	$App->convert($_REQUEST['building']);
					$data['street']			=	$App->convert($_REQUEST['street']);
					$data['zone']			=	$App->convert($_REQUEST['zone']);
					$data['feeDetails']		=	$App->convert($_REQUEST['feeDetails']);
					$data['hourNeeded']		=	$App->convert($_REQUEST['hourNeeded']);
					$data['remark']			=	$App->convert($_REQUEST['remark']);
					$data['parentName']		=	ucwords(strtolower($App->convert($_REQUEST['parentName'])));
					$data['status']			=	'Pending';
					$data['alertDate']		=	$App->dbFormat_date($_REQUEST['alertDate']);
					
												
					$success	=	$db->query_insert(TABLE_ENQUIRY,$data);								
					$db->close();
					if($success)
					{
						$_SESSION['msg']="Enquiry details added successfully";										
					}	
					else
					{
						$_SESSION['msg']="Failed";											
					}	
					header("location:new.php");		
			  }
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
		if(!$_REQUEST['studentName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
							
					$data['enquiryDate']	=	$App->dbFormat_date($_REQUEST['enquiryDate']);
					$data['studentName']	=	ucwords(strtolower($App->convert($_REQUEST['studentName'])));
					$data['studClass']		=	ucwords(strtolower($App->convert($_REQUEST['studClass'])));
					$data['subjectId']		=	$App->convert($_REQUEST['subjectId']);
					$data['phone']			=	$App->convert($_REQUEST['phone']);
					$data['school']			=	ucwords(strtolower($App->convert($_REQUEST['school'])));
					$data['location']		=	$App->convert($_REQUEST['location']);
					$data['building']		=	$App->convert($_REQUEST['building']);
					$data['street']			=	$App->convert($_REQUEST['street']);
					$data['zone']			=	$App->convert($_REQUEST['zone']);
					$data['feeDetails']		=	$App->convert($_REQUEST['feeDetails']);
					$data['hourNeeded']		=	$App->convert($_REQUEST['hourNeeded']);
					$data['remark']			=	$App->convert($_REQUEST['remark']);
					$data['parentName']		=	ucwords(strtolower($App->convert($_REQUEST['parentName'])));
					$data['status']			=	$App->convert($_REQUEST['status']);
					$data['alertDate']		=	$App->dbFormat_date($_REQUEST['alertDate']);
					
												
					$success	=	$db->query_update(TABLE_ENQUIRY,$data," ID='{$editId}'");								
					$db->close();
					if($success)
					{
						$_SESSION['msg']="Enquiry details updated successfully";										
					}	
					else
					{
						$_SESSION['msg']="Failed";											
					}	
					header("location:new.php");		
			  }
		break;		
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_ENQUIRY."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Enquiry details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");					
		break;		
}
?>