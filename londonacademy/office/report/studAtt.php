<?php include("../adminHeader.php"); ?>
<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>Attendance Report</h2>
            </div>
            <div class="col-sm-8 text-right">
              <form class="form-inline form3" method="post">
                <div class="form-group">                
                  <input type="text" class="form-control datepicker" placeholder="Date From" name="dateFrom"  value="<?php echo @$_REQUEST['dateFrom'] ?>">                    
                </div>
                 <div class="form-group">                
                  <input type="text" class="form-control datepicker" placeholder="Date To" name="dateTo"  value="<?php echo @$_REQUEST['dateTo'] ?>">                    
                </div>
                 <div class="form-group">                
                  <input type="text" class="form-control" placeholder="Name / Ad.No / Phone " value="<?php echo @$_REQUEST['sname'] ?>" name="sname">                    
                </div><br><br>
                <button type="submit" class="btn btn-default lens"></button>
              </form>
            </div>
          </div>
          <?php	
$cond="1";
if(@$_REQUEST['dateFrom'])
{
	$dateFrom=$App->dbformat_date($_REQUEST['dateFrom']);
	$cond=$cond." and ".TABLE_STUDENT_SCHEDULE.".classDate>='$dateFrom'";
}
if(@$_REQUEST['dateTo'])
{
	$dateTo=$App->dbformat_date($_REQUEST['dateTo']);
	$cond=$cond." and ".TABLE_STUDENT_SCHEDULE.".classDate<='$dateTo'";
}
if(@$_REQUEST['sname'])
{	
	$a=$_REQUEST['sname'];
	$rest = substr("$a", 2);
	$cond=$cond." and ((".TABLE_STUDENT.".name like'%".$_POST['sname']."%') or (".TABLE_STUDENT.".adNo like'%$rest%') or (".TABLE_STUDENT.".mobile like'%".$_POST['sname']."%'))";
}
?>
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table  view_limitter pagination_table" >
                  <thead>
                    <tr>
                      <th>SlNo.</th> 
                      <th>Student</th>            									                       										
                        <th>Date</th>
                        <th>Time</th>                      	
                        <th>Status</th>					
                        <th></th>		               
                    </tr>
                  </thead>
                  <tbody>
				<?php 															
							
				$today=date('Y-m-d');														
						$selAllQuery="select `".TABLE_STUDENT_SCHEDULE."`.ID,																						 											`".TABLE_STUDENT."`.name as studName,
											 `".TABLE_SUBJECT."`.subjectName,
											 `".TABLE_SUBJECT."`.place,
											 `".TABLE_SUBJECT."`.countType,
											 `".TABLE_STAFF."`.name,
											 `".TABLE_STUDENT_SCHEDULE."`.classDate,
											 `".TABLE_STUDENT_SCHEDULE."`.timeFrom,
											 `".TABLE_STUDENT_SCHEDULE."`.timeTo,
											 `".TABLE_STUDENT_ATTENDANCE."`.remark,
											 `".TABLE_STUDENT_ATTENDANCE."`.status											
										from `".TABLE_STUDENT_ATTENDANCE."` 
											right join `".TABLE_STUDENT_SCHEDULE."`
												on `".TABLE_STUDENT_SCHEDULE."`.ID=`".TABLE_STUDENT_ATTENDANCE."`.scheduleId
											right join `".TABLE_SUBJECT."`
												on `".TABLE_STUDENT_SCHEDULE."`.subjectId=`".TABLE_SUBJECT."`.ID
											right join `".TABLE_STAFF."`
												on `".TABLE_STUDENT_SCHEDULE."`.teacherId=`".TABLE_STAFF."`.ID
											right join `".TABLE_STUDENT."`
												on `".TABLE_STUDENT_SCHEDULE."`.studentId=`".TABLE_STUDENT."`.ID
									    where `".TABLE_STUDENT_SCHEDULE."`.classDate<'$today' and $cond
									    order by `".TABLE_STUDENT_SCHEDULE."`.classDate desc";
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);					
						if($number==0)
						{
						?>
                         <tr>
                            <td align="center" colspan="6">
                                There is no data in list.
                            </td>
                        </tr>
						<?php
						}
						else
						{
							$i=0;
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];							
							?>
							  <tr>
		                       	<td><?php echo ++$i;?></td>
		                       	<td><?php echo $row['studName']; ?></td> 		                       														
								<td><?php echo $App->dbformat_date($row['classDate']); ?></td>
								<?php
								$t1= $row['timeFrom'];
								$t2= $row['timeTo'];
								?>
								<td><?php echo date('h:i a',strtotime($t1))."-".date('h:i a',strtotime($t2));?></td>								
								<?php 
								if($row['status']=='')
								{
								?>
								<td>Pending</td>
								<?php	
								}												
								else
								{
								?>
									<td><?php echo $row['status']; ?></td>
								<?php	
								}
								?>																	
									<td align="center"><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>" class="viewbtn">View</a>
                                <!-- Modal3 -->
							  <div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
								<div class="modal-dialog">
								  <div class="modal-content"> 
									<!-- <div class="modal-body clearfix">
								fddhdhdhddhdh
								  </div>-->	 		
									<div role="tabpanel" class="tabarea2"> 
									  
									  <!-- Nav tabs -->
									  <ul class="nav nav-tabs" role="tablist">
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Details</a> </li>										
									  </ul>
									  
									  <!-- Tab panes -->
									  <div class="tab-content">
										<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg" >
											<tbody style="background-color:#FFFFFF">
											  <tr>
												<td>Name</td>
												<td>:</td>
												<td><?php echo $row['studName']; ?></td>
											  </tr>
											  <tr>
												<td>Subject</td>	
												<td>:</td>											
                                                <td><?php echo $row['subjectName']."-".$row['place']."-".$row['countType']; ?></td>
											  </tr>
											  <tr>
												<td>Date</td>
												<td>:</td>
												<td><?php echo $App->dbformat_date($row['classDate']); ?></td>
											  </tr>
											  <tr>
												<td>Time</td>
												<td>:</td>
												<?php
												$t1= $row['timeFrom'];
												$t2= $row['timeTo'];
												?>
												<td><?php echo date('h:i a',strtotime($t1))."-".date('h:i a',strtotime($t2));?></td>
											  </tr>
											  <tr>
												<td>Teacher</td>
												<td>:</td>
												<td><?php echo $row['name']; ?></td>
											  </tr>
											  <tr>
												<td>Status</td>
												<td>:</td>
												<?php 
												if($row['status']=='')
												{
												?>
												<td>Pending</td>
												<?php	
												}												
												else
												{
												?>
													<td><?php echo $row['status']; ?></td>
												<?php	
												}
												?>				
											  </tr>
											  <tr>
												<td>Remark</td>
												<td>:</td>
												<td><?php echo $row['remark']; ?></td>
											  </tr>											 
											</tbody>
										  </table>
										</div>
										
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							  <!-- Modal3 cls --> 
						  
                                </td>		
							  </tr>
					  <?php }
						}?>                  
                </tbody>
                </table>
              </div>
              <!-- paging -->		
            <div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div>        
            <!-- paging end-->
            </div>
          </div>
        </div>
      </div>


<?php include("../adminFooter.php"); ?>