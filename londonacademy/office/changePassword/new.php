<?php include("../adminHeader.php") ?>

<?php

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();


 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="../adminDash/admin-dash.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">CHANGE PASSWORD</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post">			
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                      <label for="oldUserName">Existing User name:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="oldUserName" id="oldUserName" required >
                    </div>
					
					<div class="form-group">
                      <label for="newUserName">New User name:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="newUserName" id="newUserName" required  >
                    </div>
				</div>	
				<div class="col-sm-6">
					<div class="form-group">
						<label for="oldPassword">Existing Password<span class="valid">*</span></label>
						<input type="password" name="oldPassword" id="oldPassword" class="form-control2" required >
					</div>
					
					<div class="form-group">
						<label for="newPassword" >New Password:<span class="valid">*</span></label></br >
						<input type="password" name="newPassword" id="newPassword" class="form-control2" required>
					</div>
					
					<div class="form-group">
						<label for="conPassword">Confirm Password:<span class="valid">*</span></label>	
						<input type="password" name="conPassword" id="conPassword" class="form-control2" required>
					</div>					
				</div>
                   </div>  				             
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
