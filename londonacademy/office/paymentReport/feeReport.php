<?php include("../adminHeader.php"); ?>
<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>Student Payment Report</h2>
            </div>
            <div class="col-sm-8 text-right">
              <form class="form-inline form3" method="post">
                <div class="form-group">                
                  <input type="text" class="form-control datepicker" placeholder="Date From" name="dateFrom"  value="<?php echo @$_REQUEST['dateFrom'] ?>">                    
                </div>
                 <div class="form-group">                
                  <input type="text" class="form-control datepicker" placeholder="Date To" name="dateTo"  value="<?php echo @$_REQUEST['dateTo'] ?>">                    
                </div>
                 <div class="form-group">                
                  <input type="text" class="form-control" placeholder="Student Name / Ad.No / Phone " value="<?php echo @$_REQUEST['sname'] ?>" name="sname">                    
                </div>
                <button type="submit" class="btn btn-default lens"></button>
              </form>
            </div>
          </div>
          <?php	
$cond="1";
if(@$_REQUEST['dateFrom'])
{
	$dateFrom=$App->dbformat_date($_REQUEST['dateFrom']);
	$cond=$cond." and ".TABLE_FEEPAYMENT.".paymentDate>='$dateFrom'";
}
if(@$_REQUEST['dateTo'])
{
	$dateTo=$App->dbformat_date($_REQUEST['dateTo']);
	$cond=$cond." and ".TABLE_FEEPAYMENT.".paymentDate<='$dateTo'";
}
if(@$_REQUEST['sname'])
{	
	$a=$_REQUEST['sname'];
	$rest = substr("$a", 2);
	$cond=$cond." and ((".TABLE_STUDENT.".name like'%".$_POST['sname']."%') or (".TABLE_STUDENT.".adNo like'%$rest%') or (".TABLE_STUDENT.".mobile like'%".$_POST['sname']."%'))";
}
?>
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table  view_limitter pagination_table" >
                  <thead>
                    <tr>
                      <td>SlNo.</td>                      
                      <td>Student Name</td>
                      <td>Amount</td>                     
                      <td>Paid Date</td>                                                                
                    </tr>
                  </thead>
                  <tbody>
				<?php 															
						$selAllQuery="select `".TABLE_FEEPAYMENT."`.ID,
											 `".TABLE_FEEPAYMENT."`.paidAmount,
											 `".TABLE_FEEPAYMENT."`.paymentDate,
											 `".TABLE_STUDENT."`.name											 									
										from `".TABLE_FEEPAYMENT."` 
											inner join `".TABLE_STUDENT."`
											 on `".TABLE_FEEPAYMENT."`.studentId=`".TABLE_STUDENT."`.ID	
											and $cond																			    	 									    
									    order by `".TABLE_FEEPAYMENT."`.paymentDate desc";	
										
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);					
						if($number==0)
						{
						?>
                         <tr>
                            <td align="center" colspan="4">
                                There is no data in list.                          
                            </td>
                        </tr>
						<?php
						}
						else
						{
							$i=0;
							while($row=mysql_fetch_array($selectAll))
							{	
							$scheduleId=$row['ID'];
							?>
							  <tr>		                       							
								<td><?php echo ++$i;?></td>                         					   
		                        <td><?php echo $row['name'];?> </td>						
								<td><?php echo $row['paidAmount']; ?></td>
								<td><?php echo $App->dbformat_date($row['paymentDate']); ?></td>								
																							
							  </tr>
							  
					  <?php }
						}?>                  
                </tbody>
                </table>
              </div>
              <!-- paging -->		
            <div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div>        
            <!-- paging end-->
            </div>
          </div>
        </div>
      </div>


<?php include("../adminFooter.php"); ?>