<?php include("../adminHeader.php"); ?>
<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>Teacher Schedule Report</h2>
            </div>
            <div class="col-sm-8 text-right">
              <form class="form-inline form3" method="post">
                <div class="form-group">                
                  <input type="text" class="form-control datepicker" placeholder="Date From" name="dateFrom"  value="<?php echo @$_REQUEST['dateFrom'] ?>">                    
                </div>
                 <div class="form-group">                
                  <input type="text" class="form-control datepicker" placeholder="Date To" name="dateTo"  value="<?php echo @$_REQUEST['dateTo'] ?>">                    
                </div>
                 <div class="form-group">                
                  <input type="text" class="form-control" placeholder="Teacher Name / Mobile" value="<?php echo @$_REQUEST['tname'] ?>" name="tname">                    
                </div>
                <button type="submit" class="btn btn-default lens"></button>
              </form>
            </div>
          </div>
          <?php	
$cond="1";
if(@$_REQUEST['tname'])
{
	//$cond=$cond." and ".TABLE_STAFF.".name like'%".$_REQUEST['tname']."%'";
	$cond=$cond." and ((".TABLE_STAFF.".name like'%".$_POST['tname']."%') or (".TABLE_STAFF.".phone like'%".$_POST['tname']."%'))";
}
if(@$_REQUEST['dateFrom'])
{
	$dateFrom=$App->dbformat_date($_REQUEST['dateFrom']);
	$cond=$cond." and ".TABLE_STUDENT_SCHEDULE.".classDate>='$dateFrom'";
}
if(@$_REQUEST['dateTo'])
{
	$dateTo=$App->dbformat_date($_REQUEST['dateTo']);
	$cond=$cond." and ".TABLE_STUDENT_SCHEDULE.".classDate<='$dateTo'";
}
?>
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table view_limitter pagination_table" >
                  <thead>
                    <tr>
                      <td>SlNo.</td>
                      <td>Teacher</td> 
                      <td>Student</td>                                                              
                      <td>Subject</td>                      
                      <td>Date</td>
                      <td>Time</td>                      
                    </tr>
                  </thead>
                  <tbody>
				<?php 															
						$selAllQuery="select `".TABLE_STUDENT."`.name as studName,
											`".TABLE_SUBJECT."`.subjectName,
											 `".TABLE_SUBJECT."`.place,
											 `".TABLE_SUBJECT."`.countType,
											 `".TABLE_STAFF."`.name,
											 `".TABLE_STUDENT_SCHEDULE."`.classDate,
											 `".TABLE_STUDENT_SCHEDULE."`.timeFrom,
											 `".TABLE_STUDENT_SCHEDULE."`.timeTo											  
										from `".TABLE_STUDENT_SCHEDULE."`,`".TABLE_SUBJECT."`,`".TABLE_STAFF."`,`".TABLE_STUDENT."` 
									   where `".TABLE_STUDENT_SCHEDULE."`.studentId=`".TABLE_STUDENT."`.ID
									   	and  `".TABLE_STUDENT_SCHEDULE."`.teacherId=`".TABLE_STAFF."`.ID
									   	and `".TABLE_STUDENT_SCHEDULE."`.subjectId=`".TABLE_SUBJECT."`.ID
									   	and $cond
									   	order by `".TABLE_STUDENT_SCHEDULE."`.classDate desc";
										
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);					
						if($number==0)
						{
						?>
                         <tr>
                            <td align="center" colspan="7">
                                There is no data in list.
                            </td>
                        </tr>
						<?php
						}
						else
						{
							$i=0;
							while($row=mysql_fetch_array($selectAll))
							{	
							$name=$row['name'];
							?>
							  <tr>
		                       	<td><?php echo ++$i;?></td>
		                       	<td><?php echo $row['name']; ?></td>
		                       	<td><?php echo $row['studName']; ?></td>                        					   								
		                        <td><?php echo $row['subjectName']."-".$row['place']."-".$row['countType']; ?></td>														
								<td><?php echo $App->dbformat_date($row['classDate']); ?></td>
								<td><?php echo $row['timeFrom']."-".$row['timeTo']; ?></td>
								
							  </tr>
					  <?php }
						}?>                  
                </tbody>
                </table>
              </div>
              <!-- paging -->		
            <div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div>        
            <!-- paging end-->
            </div>
          </div>
        </div>
      </div>

</<div>
<?php include("../adminFooter.php"); ?>