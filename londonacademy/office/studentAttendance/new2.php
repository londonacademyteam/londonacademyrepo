<?php include("../adminHeader.php"); ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$loginType	=	$_SESSION['LogType'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
$studId		=	$_REQUEST['id'];
$select2	=	mysql_query("select name from ".TABLE_STUDENT." where ID='$studId'");
$res2		=	mysql_fetch_array($select2);
$studentName =	$res2['name'];

?>

<script>
function changeStatus()
{
var del=confirm("Do you want to change attendance ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">STUDENT ATTENDANCE</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">           
              <table class="table view_limitter pagination_table">
                <caption class="names"><a href="new.php" class="back_btn"><i class="fa fa-reply"></i></a>Name: <?php echo $studentName ?></caption>
                <thead>
                  <tr> 
                  	<th>SlNo.</th>             									
                    <th>Subject</th>										
					<th>Date</th>
					<th>Time</th>
					<th>Teacher</th>	
					<th>Status</th>					
					<th>Remark</th>															
                  </tr>
                </thead>
                <tbody>
				<?php 	
				$today=date('Y-m-d');														
						$selAllQuery="select `".TABLE_STUDENT_SCHEDULE."`.ID,																						 
											 `".TABLE_SUBJECT."`.subjectName,
											 `".TABLE_SUBJECT."`.place,
											 `".TABLE_SUBJECT."`.countType,
											 `".TABLE_STAFF."`.name,
											 `".TABLE_STUDENT_SCHEDULE."`.classDate,
											 `".TABLE_STUDENT_SCHEDULE."`.timeFrom,
											 `".TABLE_STUDENT_SCHEDULE."`.timeTo,
											 `".TABLE_STUDENT_ATTENDANCE."`.remark,
											 `".TABLE_STUDENT_ATTENDANCE."`.status											
										from `".TABLE_STUDENT_ATTENDANCE."` 
											right join `".TABLE_STUDENT_SCHEDULE."`
												on `".TABLE_STUDENT_SCHEDULE."`.ID=`".TABLE_STUDENT_ATTENDANCE."`.scheduleId											
											right join `".TABLE_SUBJECT."`
												on `".TABLE_STUDENT_SCHEDULE."`.subjectId=`".TABLE_SUBJECT."`.ID
											right join `".TABLE_STAFF."`
												on `".TABLE_STUDENT_SCHEDULE."`.teacherId=`".TABLE_STAFF."`.ID
									    where `".TABLE_STUDENT_SCHEDULE."`.studentId='$studId' 
									    	and `".TABLE_STUDENT_SCHEDULE."`.classDate<'$today'
									    order by `".TABLE_STUDENT_SCHEDULE."`.classDate desc";
												
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);					
						if($number==0)
						{
						?>
                         <tr>
                            <td align="center" colspan="7">
                                There is no data in list.                          
                            </td>
                        </tr>
						<?php
						}
						else
						{
							$i=0;
							while($row=mysql_fetch_array($selectAll))
							{	
							$scheduleId=$row['ID'];
							$status=$row['status'];
							?>
							  <tr>		                       							
								<td><?php echo ++$i;?>
								<?php if(($loginType=='admin')&&($status!='')){?>
									<div class="adno-dtls"> <a href="edit.php?id=<?php echo $studId?>&sid=<?php echo $scheduleId?>">EDIT</a></div></td>
								<?php	
									}
		                       		?>

								</td>                         					   
								<td><?php echo $row['subjectName']."-".$row['place']."-".$row['countType']; ?></td>														
								<td><?php echo $App->dbformat_date($row['classDate']); ?></td>
								<?php
								$t1= $row['timeFrom'];
								$t2= $row['timeTo'];
								?>
								<td><?php echo date('h:i a',strtotime($t1))."-".date('h:i a',strtotime($t2));?></td>
								<td><?php echo $row['name']; ?></td>
								<?php 
								if($row['status']=='Present')
								{
								?>
								<td><a href="do.php?id=<?php echo $scheduleId;?>&op=p&studId=<?php echo $studId;?>" style="color: #03fc16"  onclick="return changeStatus();">Present</a></td>
								<?php	
								}
								elseif($row['status']=='Absent')
								{
								?>
									<td><a href="do.php?id=<?php echo $scheduleId;?>&op=a&studId=<?php echo $studId;?>" style="color: #fc2803; text-decoration: underline;"  onclick="return changeStatus();">Absent</a></td>
								<?php
								}
								else
								{
								?>
									<td>Pending</td>
								<?php	
								}
								?>								
									<td><?php echo $row['remark']; ?></td>																
							  </tr>
					  <?php }
						}?>                  
                </tbody>
              </table>			  	  	
            </div>
             <!-- paging -->		
            <div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div>        
            <!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
<!--              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
              <h4 class="modal-title">STUDENT ATTENDANCE</h4>
            </div><br>
            <!--<div class="col-sm-12" align="center">
	            <form class="form-inline form3" method="post">
	                <div class="form-group">                
	                  <input type="text" class="form-control datepicker" placeholder="Date From" name="dateFrom"  value="<?php echo @$_REQUEST['dateFrom'] ?>" style="background-color: #e3e3e1">                    
	                </div>
	                 <div class="form-group">                
	                  <input type="text" class="form-control datepicker" placeholder="Date To" name="dateTo"  value="<?php echo @$_REQUEST['dateTo'] ?>"  style="background-color: #e3e3e1">                    
	                </div>
	                 
	                <button type="submit" class="btn btn-default lens"></button>
	              </form>
              </div>-->
              <div style="clear: both;"></div>
<?php
$cond="1";
if(@$_REQUEST['dateFrom'])
{
	$dateFrom=$App->dbformat_date($_REQUEST['dateFrom']);
	$cond=$cond." and ".TABLE_STUDENT_SCHEDULE.".classDate>='$dateFrom'";
}
if(@$_REQUEST['dateTo'])
{
	$dateTo=$App->dbformat_date($_REQUEST['dateTo']);
	$cond=$cond." and ".TABLE_STUDENT_SCHEDULE.".classDate<='$dateTo'";
}
?>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
              <input type="hidden" name="studId" id="studId" value="<?php echo $studId ?>">
                <div class="row">
                 <div class="col-sm-12">
                   <!--<div class="col-md-9 col-sm-12">-->
                     	<table class="table view_limitter pagination_table">
                    		<thead style="background-color: #e3e3e1">
			                  <tr> 			                  	
								<th>SlNo.</th>             										
			                    <th>Subject</th>										
								<th>Date</th>
								<th>Time</th>	
								<th>Present</th>
								<th>Absent</th>	
								<th>Pending</th>
								<th>Remark</th>																			
			                 </tr>			                 
			                </thead>
			                <tbody>
						<?php 																									
						$selAllQuery="select `".TABLE_STUDENT_SCHEDULE."`.ID,
											 `".TABLE_SUBJECT."`.subjectName,
											 `".TABLE_SUBJECT."`.place,
											 `".TABLE_SUBJECT."`.countType,
											 `".TABLE_STAFF."`.name,
											 `".TABLE_STUDENT_SCHEDULE."`.classDate,
											 `".TABLE_STUDENT_SCHEDULE."`.timeFrom,
											 `".TABLE_STUDENT_SCHEDULE."`.timeTo,
											 `".TABLE_STUDENT_ATTENDANCE."`.status											
										from `".TABLE_STUDENT_ATTENDANCE."` 
											right join `".TABLE_STUDENT_SCHEDULE."`
												on `".TABLE_STUDENT_SCHEDULE."`.ID=`".TABLE_STUDENT_ATTENDANCE."`.scheduleId
											right join `".TABLE_SUBJECT."`
												on `".TABLE_STUDENT_SCHEDULE."`.subjectId=`".TABLE_SUBJECT."`.ID											
											right join `".TABLE_STAFF."`
												on `".TABLE_STUDENT_SCHEDULE."`.teacherId=`".TABLE_STAFF."`.ID
									    where `".TABLE_STUDENT_SCHEDULE."`.studentId='$studId' 
									    	and `".TABLE_STUDENT_SCHEDULE."`.classDate<'$today'									    
									    order by `".TABLE_STUDENT_SCHEDULE."`.classDate desc";
										//echo $selAllQuery;		
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);				
						if($number==0)
						{
						?>
                         <tr>
                            <td align="center" colspan="8">
                                There is no data in list.
                            </td>
                        </tr>
						<?php
						}
						else
						{
							$i=0;
							while($row=mysql_fetch_array($selectAll))
							{	
							$status=$row['status'];
							$scheduleId=$row['ID'];
								if($status=='')
								{
									?>
							  <tr>
		                       	<td><?php echo ++$i;?></td>                         									
		                        <td><?php echo $row['subjectName'];?> </td>														
								<td><?php echo $App->dbformat_date($row['classDate']); ?></td>
								<?php
								$t1= $row['timeFrom'];
								$t2= $row['timeTo'];
								?>
								<td><?php echo date('h:i a',strtotime($t1))."-".date('h:i a',strtotime($t2));?></td>													
								<td><input type="radio" name="att<?php echo $i ?>" id="att<?php echo $i ?>" value="Present"></td>
								<td><input type="radio" name="att<?php echo $i ?>" id="att<?php echo $i ?>" value="Absent"></td>
								<td><input type="radio" name="att<?php echo $i ?>" id="att<?php echo $i ?>" value="Pending" checked="checked">
									<input type="hidden" name="scheduleId<?php echo $i ?>" id="scheduleId<?php echo $i ?>" value="<?php echo $scheduleId ?>">
								</td>	
								<td><textarea name="remark<?php echo $i ?>" id="remark<?php echo $i ?>"></textarea></td>								
							  </tr>
							  	
					 	 <?php }
							}
							?>
							<input type="hidden" name="leng" id="leng" value="<?php echo $i ?>">
							<?php      
						}
						?>
							           
                </tbody>
                    	</table>
                    
                    
                   </div>					
                 </div>                  
                </div>              
			  <div
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls -->      
      </div>
  </div>
 
<?php include("../adminFooter.php") ?>
