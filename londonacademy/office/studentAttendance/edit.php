<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
globalVar=0;
function durationType(feeType)
{
	if(feeType!='Package')
	{	
	globalVar=1;
	document.getElementById('notPackageDiv').style.display='block';
	document.getElementById('packageDiv').style.display='none';
	document.getElementById('durationTyp').value=feeType;
	document.getElementById('duration').setAttribute("required","required");
	document.getElementById('durationP').removeAttribute("required","required");
	document.getElementById('durationTy').removeAttribute("required","required");
	}
	else
	{
	globalVar=2;
	document.getElementById('packageDiv').style.display='block';	
	document.getElementById('notPackageDiv').style.display='none';
	document.getElementById('durationP').setAttribute("required","required");
	document.getElementById('durationTy').setAttribute("required","required");
	document.getElementById('duration').removeAttribute("required","required");
	}
}
//Validation
function valid()
{
flag=false;
	fee=document.getElementById('fee').value;
		if(isNaN(fee))
		{
		document.getElementById('f').innerHTML="Enter number only";
		flag=true;
		}
		
	regFee=document.getElementById('regFee').value;
		if(isNaN(regFee))
		{
		document.getElementById('rf').innerHTML="Enter number only";
		flag=true;
		}
		
	if(globalVar==1)
	{
	duration=document.getElementById('duration').value;
		if(isNaN(duration))
		{
		document.getElementById('d').innerHTML="Enter number only";
		flag=true;
		}
	}
	else
	{
	durationP=document.getElementById('durationP').value;
		if(isNaN(durationP))
		{
		document.getElementById('dp').innerHTML="Enter number only";
		flag=true;
		}	
	}
	
	if(flag==true)
	{
	return false;
	}
}
//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

function hideRemark()
{
status	=	document.getElementById('status').value;
	if(status=='')
	{
		document.getElementById('remarkDiv').style.display='none';
	}
	else
	{
		document.getElementById('remarkDiv').style.display='block';
	}		
}
</script>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$studId=$_REQUEST['id'];
	$editId=$_REQUEST['sid'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_STUDENT_ATTENDANCE."` WHERE scheduleId='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">ATTENDANCE</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $studId ?>">
			  <input type="hidden" name="sid" id="sid" value="<?php echo $editId ?>">
                <div class="row">
                  <div class="col-sm-6">
                    
                    <div class="form-group">
                      <label for="status">Status:<span class="valid">*</span></label>
                        <select name="status" id="status" class="form-control2" required onchange="hideRemark()">
						  <option value="Present" <?php if($editRow['status']=='Present'){?> selected="selected"<?php }?>>Present</option>
						  <option value="Absent" <?php if($editRow['status']=='Absent'){?> selected="selected"<?php }?>>Absent</option>
						  <option value="">Pending</option>
						</select>	
                    </div>
                    <div id="remarkDiv">
	                    <div class="form-group">
	                      <label for="remark">Remark:<span class="valid">*</span></label>
	                      <textarea name="remark" id="remark" class="form-control2"><?php echo $editRow['remark']?></textarea>
	                    </div> 
                    </div>                                                      
                </div>              
			  <div>
            </div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
