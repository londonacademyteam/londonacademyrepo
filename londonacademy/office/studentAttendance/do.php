<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['studId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new2.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$studId		=	$App->convert($_REQUEST['studId']);
				$k=1;
				for($k=1;$k<=$_POST['leng'];$k++)
					{												 
					   $s="scheduleId".$k;
					   $a="att".$k;
					   $r="remark".$k;
					  
					   $scheduleId = $_REQUEST[$s];
					   $attendance = $_REQUEST[$a];
					   $remark 	   = $_REQUEST[$r];
					   					   					   					   					   
					  $data['scheduleId']		=	$App->convert($scheduleId);					  
			 		  $data['status']		    =	$App->convert($attendance);
			 		  $data['remark']		    =	$App->convert($remark);
			 		  $status		    		=	$App->convert($attendance);
					  if($status=='Pending')
					  {
					  	continue;
					  }
					  else{
					  	$db->query_insert(TABLE_STUDENT_ATTENDANCE,$data);
					  }																										
					}
				$db->close();
				$_SESSION['msg']="Attendance Added Successfully";					
				header("location:new2.php?id=$studId");	
														
			}		
	break;	
	//edit present to absent	
	case 'p':
			$scheduleId	=	$_REQUEST['id'];
			$studId		=	$_REQUEST['studId'];
		if(!$_REQUEST['id'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
								
			 	$data['status']		    =	'Absent';
				$s=$db->query_update(TABLE_STUDENT_ATTENDANCE,$data,"scheduleId='{$scheduleId}'");												
				$_SESSION['msg']="Attendance Updated Successfully";					
				header("location:new2.php?id=$studId");	
				$db->close();										
			}		
		break;
	//edit absent to present	
	case 'a':
			$scheduleId=$_REQUEST['id'];
			$studId		=	$_REQUEST['studId'];
		if(!$_REQUEST['id'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
								
			 	$data['status']		    =	'Present';
				$s=$db->query_update(TABLE_STUDENT_ATTENDANCE,$data,"scheduleId='{$scheduleId}'");												
				$_SESSION['msg']="Attendance Updated Successfully";					
				header("location:new2.php?id=$studId");	
				$db->close();										
			}		
		break;	
	// EDIT SECTION
	case 'edit':
		
		$studId	=	$_REQUEST['id']; 
		$editId	=	$_REQUEST['sid']; 
		if(!$_REQUEST['id'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new2.php?id=$studId");			
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
																		
				$status						=	$App->convert($_REQUEST['status']);				
				 if($status=='')
				 {					 			
				 	$success	= 	@mysql_query("DELETE FROM `".TABLE_STUDENT_ATTENDANCE."` WHERE scheduleId='{$editId}'");
				 }
				 else{
				 	$data['status']				=	$App->convert($_REQUEST['status']);	
				 	$data['remark']				=	$App->convert($_REQUEST['remark']);	
				 	$success	=	$db->query_update(TABLE_STUDENT_ATTENDANCE,$data,"scheduleId='{$editId}'");	
				 }
											
				$db->close();
				if($success)
				{
					$_SESSION['msg']="Attendance details updated successfully";										
				}	
				else
				{
					$_SESSION['msg']="Failed";											
				}	
				header("location:new2.php?id=$studId");										
			}								
break;			
}
?>