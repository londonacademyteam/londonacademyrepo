<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		if(!$_REQUEST['studId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new2.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$studId					=	$App->convert($_REQUEST['studId']);	
				$data['studentId']		=	$App->convert($studId);				
			 	$data['paidAmount']		=	$App->convert($_REQUEST['paidAmount']);
			 	$data['discount']		=	$App->convert($_REQUEST['discount']);
			 	$data['paymentDate']	=	date('Y-m-d');
												
				$db->query_insert(TABLE_FEEPAYMENT,$data);												
				$db->close();
				$_SESSION['msg']="Fee paid Successfully";					
				header("location:new2.php?id=$studId");	
														
			}		
	break;
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$studentId	=	$App->convert($_REQUEST['sid']);				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_FEEPAYMENT."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Payment deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new2.php?id=$studentId");
								
		break;			
	
}
?>