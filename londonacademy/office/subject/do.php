<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['subjectName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
				
				$data['subjectName']		=	ucwords(strtolower($App->convert($_REQUEST['subjectName'])));			
				$data['place']				=	$App->convert($_REQUEST['place']);	
				$data['countType']			=	$App->convert($_REQUEST['countType']);	
				$data['fee']				=	$App->convert($_REQUEST['fee']);
				
				$success	=	$db->query_insert(TABLE_SUBJECT,$data);								
				$db->close();
				if($success)
				{
					$_SESSION['msg']="Subject Details Added Successfully";										
				}	
				else
				{
					$_SESSION['msg']="Failed";											
				}	
				header("location:new.php");									
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
		if(!$_REQUEST['subjectName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
				
				$data['subjectName']		=	ucwords(strtolower($App->convert($_REQUEST['subjectName'])));			
				$data['place']				=	$App->convert($_REQUEST['place']);	
				$data['countType']			=	$App->convert($_REQUEST['countType']);	
				$data['fee']				=	$App->convert($_REQUEST['fee']);
				
				$success	=	$db->query_update(TABLE_SUBJECT,$data,"ID='{$editId}'");								
				$db->close();
				if($success)
				{
					$_SESSION['msg']="Subject Details updated Successfully";										
				}	
				else
				{
					$_SESSION['msg']="Failed";											
				}	
				header("location:new.php");									
			}								
		break;		
	// DELETE SECTION	
		case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_SUBJECT."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Subject details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");		
		break;			
}
?>