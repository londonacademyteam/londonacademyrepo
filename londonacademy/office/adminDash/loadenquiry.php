<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

 
 		<div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">ENQUIRY DETAILS</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="../enquiry/do.php?op=new" class="form1" method="post" onsubmit="return valid()">
              <input type="hidden" name="toIndex" id="toIndex" value="1"> <!--after inserting should return to index page-->
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="enquiryDate">Enquiry Date:<span class="valid">*</span></label>
                      <input type="text" name="enquiryDate" id="enquiryDate" class="form-control2 datepicker" required value="<?php echo date('d/m/Y') ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label for="studentName">Student Name:<span class="valid">*</span></label>
                        <input type="text" class="form-control2" name="studentName" id="studentName" required>	
                    </div>
                    <div class="form-group">
                      <label for="studClass">Class:</label>
                      <input type="text" class="form-control2" name="studClass" id="studClass" >
                    </div>
                    <div class="form-group">
                      <label for="subjectId">Subject:<span class="valid">*</span></label>
                      <select name="subjectId" id="subjectId" class="form-control2" required  onchange="getFee(this.value)">
							<option value="">Select</option>
							<?php 
							$select2="select * from ".TABLE_SUBJECT." order by subjectName";
							$res2=mysql_query($select2);
							while($row2=mysql_fetch_array($res2))
							{
							?>
							<option value="<?php echo $row2['ID']?>"><?php echo $row2['subjectName']." - ".$row2['place']." - ".$row2['countType'];?></option>
							<?php									
							}
							?>				
						</select>
                    </div>
                    <div class="form-group">
                      <label for="feeDetails">Fee Details:</label>
                      <input type="text" class="form-control2" name="feeDetails" id="feeDetails" >					 
                    </div>
                     <div class="form-group">
                      <label for="hourNeeded">Hour Needed:</label>
                      <input type="text" class="form-control2" name="hourNeeded" id="hourNeeded" >					 
                    </div>
                  
                    <div class="form-group">
                      <label for="phone">Phone:<span class="valid">*</span></label>
                      <input type="text" name="phone" id="phone" class="form-control2" >	
                    </div>					
                              					
                    <div class="form-group">
                      <label for="School">School:</label>
                      <input type="text" name="school" id="school" class="form-control2">
                    </div>
				</div>
                 <div class="col-sm-6">	    
                    <div class="form-group">
                      <label for="location">Location:</label>
                      <input type="text" name="location" id="location" class="form-control2">
                    </div>
                    <div class="form-group">
                      <label for="building">Building:</label>
                      <input type="text" name="building" id="building" class="form-control2" >			  
                    </div>
                    <div class="form-group">
                      <label for="street">Street:</label>
                      <input type="text" name="street" id="street" class="form-control2" >			  
                    </div>
                    <div class="form-group">
                      <label for="zone">Zone:</label>
                      <input type="text" name="zone" id="zone" class="form-control2" >			  
                    </div>								 
                  	<div class="form-group">
                      <label for="parentName">Parent Name:</label>
                      <input type="text" name="parentName" id="parentName" class="form-control2">
                    </div>							 
                  				
                    <div class="form-groupy">
                      <label for="remark">Remark:</label>
                      <textarea id="remark" name="remark" class="form-control2"></textarea>			  
                    </div>
                    <div class="form-group">
                      <label for="alertDate">Alert Date:<span class="valid">*</span></label>
                      <input type="text" name="alertDate" id="alertDate" class="form-control2 datepicker" required value="<?php echo date('d/m/Y') ?>" readonly>
                    </div>					
                   </div> 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
		 