<?php 
include("../adminHeader.php") ;
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId = $_SESSION['LogID'];
$loginType = $_SESSION['LogType'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$select=mysql_query("select ".TABLE_STUDENT_SCHEDULE.".classDate,
							".TABLE_STAFF.".name,
							".TABLE_STUDENT_SCHEDULE.".timeFrom,
							".TABLE_STUDENT_SCHEDULE.".timeTo,
							".TABLE_SUBJECT.".subjectName,
							".TABLE_STUDENT.".name as studName,
							".TABLE_STUDENT.".studColor
					from ".TABLE_STUDENT_SCHEDULE.",".TABLE_STAFF.",".TABLE_SUBJECT.",".TABLE_STUDENT."
					where ".TABLE_STUDENT_SCHEDULE.".teacherId=".TABLE_STAFF.".ID 
						and ".TABLE_STUDENT_SCHEDULE.".studentId=".TABLE_STUDENT.".ID
						and `".TABLE_STUDENT_SCHEDULE."`.subjectId=`".TABLE_SUBJECT."`.ID");	
$results = array();
while($row=mysql_fetch_array($select))
		{
			$results[] = $row;
		}
$leng=mysql_num_rows($select);

?>
		
         
<link rel='stylesheet' href='agenda/lib/cupertino/jquery-ui.min.css' />
<link href='agenda/fullcalendar.css' rel='stylesheet' />
<link href='agenda/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='agenda/lib/moment.min.js'></script>
<script src='agenda/lib/jquery.min.js'></script>
<script src='agenda/fullcalendar.min.js'></script>
<script src='agenda/lang-all.js'></script>
<script>

	$(document).ready(function() {
		
		var currentLangCode = 'en';

		// build the language selector's options
		$.each($.fullCalendar.langs, function(langCode) {
			$('#lang-selector').append(
				$('<option/>')
					.attr('value', langCode)
					.prop('selected', langCode == currentLangCode)
					.text(langCode)
			);
		});

		// rerender the calendar when the selected option changes
		$('#lang-selector').on('change', function() {
			if (this.value) {
				currentLangCode = this.value;
				$('#calendar').fullCalendar('destroy');
				renderCalendar();
			}
		});
		
		function renderCalendar() {
		$('#calendar').fullCalendar({
			theme: true,
			header: {
				left: false,
				center: false,
				right: 'title'
			},
			height: 213,			
			defaultDate: '<?php echo date("Y-m-d"); ?>',
			editable: false,
			lang: currentLangCode,
			eventLimit: true, // allow "more" link when too many events
			events: [
			<?php for($i=0;$i< $leng;$i++)
				{
					$color		=	$results[$i][6];
					$dateFrom	=	$results[$i][0];	
					$dateTo		=	$results[$i][0];
					$dateTo 	= 	date('Y-m-d', strtotime($dateTo .' +1 day'));	
			
					$begin = new DateTime($dateFrom);
					$end = new DateTime($dateTo);
	
					$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
					foreach($daterange as $date)
					{
						$dt[$i]=$date->format("Y-m-d");
						
						 /*$start=explode(':',$results[$i][2]); 
						if($start[0]<10){
							$start[0]='0'.$start[0];} 
						if($start[2]=='PM'){
							$start[0]=$start[0]+12;}
						if(isset($start[1])){
							$startDate = $dt[$i].'T'.$start[0].':'.$start[1].':00';}
						else{
							$startDate = $dt[$i].'T'.$start[0].':00:00';}					
															
					  $end=explode(':',$results[$i][3]);						
						if($end[0]<10){
							$end[0]='0'.$end[0];} 
						if($end[2]=='PM'){
							$end[0]=$end[0]+12;}
						if(isset($end[1])){
							$endDate = $dt[$i].'T'.$end[0].':'.$end[1].':00';} 
						else{
							$endDate =  $dt[$i].'T'.$start[0].':00:00';};		*/	
							
							$startDate = $dt[$i].'T'.$results[$i][2];
							$endDate = $dt[$i].'T'.$results[$i][3];							
						?>
						{
							overlap: false, 			
							//title: "<?php echo $results[$i][1].'-'.$results[$i][5].'-'.$results[$i][4]; ?>",
							title: "<?php echo $results[$i][5].'-'.$results[$i][4]; ?>",
							start : '<?php echo $startDate; ?>',
							//start : '2015-12-22T10:30:00',
							end : '<?php echo $endDate; ?>',
							//end : '2015-12-23T11:30:00'
							color : '<?php echo $color;?>'													
						},
						<?php
					}
				}
						?>
			]
		});
		
		}
		renderCalendar();
		
	});

</script>

<script>

// ajax for loadin enquiry form
function loadEnquiry()
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("enquiryDiv").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","loadenquiry.php",true);
xmlhttp.send();
}
// ajax for loadin student 
function loadStudents()
{

	var xmlhttp;
	var sname	=	document.getElementById('sname').value;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("studentDiv").innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","loadstudent.php?sname="+sname,true);
	xmlhttp.send();

}
</script>
<script>
		 //get fee
 function getFee(str)
 {	
		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    document.getElementById("feeDetails").value=xmlhttp.responseText;
		    }
		  }
		  xmlhttp.open("GET","../enquiry/getFeeAjax.php?subject="+str,true);
		
		xmlhttp.send();
 }
 </script>
 
<style>
	body {
		
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	
	#top {
		background: #eee;
		border-bottom: 1px solid #ddd;
		padding: 0 10px;
		line-height: 40px;
		font-size: 13px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}
	.tablearea3 td {
    padding: 18px 15px !important;}

</style>

	<div class="col-md-10 col-sm-8 rightarea">
		<div style="text-align: right; margin-bottom: 15px;">
            <form method="post" style="display: inline-block; max-width: 290px;">
               <div class="input-group">
			  		<input type="text" class="form-control" id="sname"  name="sname" placeholder="Student Name / Ad.No / Phone " value="<?php echo @$_REQUEST['sname'] ?>"/>      
	                <span class="input-group-btn">
	               	<!-- <button class="btn btn-default lens" type="submit"></button>-->
	                <input class="btn btn-default lens" type="button" onclick="loadStudents()" data-toggle="modal" data-target="#myModalstudent"></button>
	               	<!--</a>-->
	                </span> 
               </div>
            </form>
         </div>
		<div class="row spec_pad">
			<div class="col-sm-3">
			<?php
          if($loginType=='teacher')
			{
			?>
				<a class="blu_box" style="background-color: #5cb85c;" href="#">
			<?php
			}
			else
			{			
          	?>
          	<a href="#" class="blu_box" style="background-color: #5cb85c;" data-toggle="modal" data-target="#myModal" >
          				 
	        <?php
	        }
	        ?>
					<h4>
						<?php $select1=mysql_query("select count(*) as TotalStu from `".TABLE_STUDENT."`");
					$editRow=mysql_fetch_array($select1);
					echo $editRow['TotalStu'];
			 			?>
					</h4>Students
				</a>
				
			</div>
			<div class="col-sm-3">
			<?php
          if($loginType=='teacher')
			{
			?>
				<a class="blu_box" href="#" style="background-color: #31b0d5;">
			<?php
			}
			else
			{			
          	?>
          	<a class="blu_box" href="#" style="background-color: #31b0d5;" data-toggle="modal" data-target="#myModalEnquiry" onclick="loadEnquiry()">
	        <?php
	        }
	        ?>
					<h4>
						<?php $select1=mysql_query("select count(*) as TotalEqy from `".TABLE_ENQUIRY."`");
					$editRow=mysql_fetch_array($select1);
					echo $editRow['TotalEqy'];
			 			?>
					</h4>Enquiry
				</a>
			</div>
			<div class="col-sm-3">
				<a class="blu_box" style="background-color: #ec971f;" href="../studentSchedule/studentToday.php">
					Today's<br />Student<br />Schedule
				</a>
			</div>
			<div class="col-sm-3">
				<a class="blu_box" style="background-color: #c9302c;" href="../staffSchedule/staffToday.php">
					Today's<br />Teacher's<br />Schedule
				</a>
			</div>
			
		</div>
		
        
		<div class="row contentarearight-btm spec_pad" style="margin-top: 8px;">
			<div class="col-lg-6 col-md-6 col-sm-6">
			<!--******calender*******-->
				<div class="calendar">
					<div class="student-shedule">
          <div class="row cal_head">
            <div class="col-sm-6">
              <h2>STUDENT CALENDAR</h2>
            </div>              
          </div>
          <div class="row">
            <div class="col-sm-12">
            
              <div class="home_cal">
        
		
		<!--    Calender   -->
						<div id='calendar'><div id='top'>
					
							Language:
							<select id='lang-selector'></select>
					
						</div></div>
		
			 
              </div>
            </div>
          </div>
        </div>
				</div>
				</div>
			 
			<!--******calender cls*******-->
			<div class="col-lg-6 col-md-6 col-sm-6">
				<ul class="clearfix">
                     <li>
                      <div class="contentarearight-btmHeading clearfix" style="background-color: #849ca8; color: #FFF;">
                        <h3 style="color: #FFF;">Enquiry Alerts</h3>
                        <a href="../alertView/new.php" class="viewall viewall_cust">View All</a>
                      </div>
                     </li>
                      <?php 
                      $today=date('Y-m-d');
					  $select=mysql_query("select studentName,alertDate,ID 
					  						from `".TABLE_ENQUIRY."` 
					  						where alertDate>='$today' 
					  						and status='Pending'
					  						order by alertDate limit 0,3");
					  						
						$num=mysql_num_rows($select);
						if($num==0)
						{
					  ?>
                           
                     <li class="clearfix">
                      <span class="eventdatebox">
                        22
                        <span class="eventdateboxday">Apr-2015</span>
                      </span>
                      <p>
                       Lorem Ipsum is simply dummy
                       text of the printing  text of the printing 
                      </p>
                     </li>
                     <li class="clearfix">
                      <span class="eventdatebox">
                        22
                        <span class="eventdateboxday">Apr-2015</span>
                      </span>
                      <p>
                       Lorem Ipsum is simply dummy
                       text of the printing  text of the printing 
                      </p>
                     </li>
                     <li class="clearfix">
                      <span class="eventdatebox">
                        22
                        <span class="eventdateboxday">Apr-2015</span>
                      </span>
                      <p>
                       Lorem Ipsum is simply dummy
                       text of the printing  text of the printing 
                      </p>
                     </li>
                     <li class="clearfix">
                      <span class="eventdatebox">
                        22
                        <span class="eventdateboxday">Apr-2015</span>
                      </span>
                      <p>
                       Lorem Ipsum is simply dummy
                       text of the printing  text of the printing 
                      </p>
                     </li>
                     <li class="clearfix">
                      <span class="eventdatebox">
                        22
                        <span class="eventdateboxday">Apr-2015</span>
                      </span>
                      <p>
                       Lorem Ipsum is simply dummy
                       text of the printing  text of the printing 
                      </p>
                     </li>
                     <?php
                        }
						else
						{
							$i=0;
							while($row=mysql_fetch_array($select))
							{															 								
								$tableId		=	$row['ID'];
								$studentName	=	$row['studentName'];
								$alertDate		=	$row['alertDate'];								
								$date1			=	date_create($alertDate);
								$today			=	date('Y-m-d');
								$date2			=	date_create($today);
								$diff			=	date_diff($date2,$date1);
								$dateDiff		=	$diff->format("%a");
								$dateSign		=	$diff->format("%R");
								if($dateSign=='+')
								{ 
									$i++;
									if($i>5)
									{
									?>
										 <a href="../alertView/new.php?" class="notifi-date" style="text-align:center; " >+MORE</a>
									<?php
									break;
									} 
							 $alert2 = date('d/M/Y', strtotime($alertDate));
							 $alert1 = explode("/",$alert2);									
									?>
								<li class="clearfix">
			                      <span class="eventdatebox">
			                        <?php echo $alert1[0];?>
			                        <span class="eventdateboxday"><?php echo $alert1[1]." - ".$alert1[2];?></span>
			                      </span>
			                      <p>                                      
								   <a href="../alertView/new.php?id=<?php echo $tableId;?>" class="notifi-date" style="text-align:center;font-size:16px; " ><?php echo "Follow up is needed for $studentName on ".$App->dbformat_date($alertDate);?></a>
								   <!-- <span class="notifi-date">Apr 22</span>-->
								  </p>  
			                     </li>
			                    <?php	
								}																		                             	
							}                      
						}
			 			?>
                      
                   </ul>
			</div>
		</div>
	</div>
	
	<!-- Modal1  for student form-->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" >
			<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <h4 class="modal-title">STUDENT REGISTRATION</h4>
			</div>
			<div class="modal-body clearfix" >
				<form action="../student/do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
				  <input type="hidden" name="joinId" id="joinId" value="0">
				  <input type="hidden" name="toIndex" id="toIndex" value="1"> <!--after inserting should return to index page-->
				    <div class="row">
				      <div class="col-sm-6">
				       <div>
				          <label for="classStart"  style="color: #1c03fc">Personal Details</label>                     
				        </div>  
				        <div class="form-group">
				          <label for="name">Name:<span class="valid">*</span></label>
				          <input type="text" class="form-control2" name="name" id="name" required>
				        </div>
				        <?php 							
								$res=mysql_query("select MAX(adNo) as a from ".TABLE_STUDENT."");
				  				$row=mysql_fetch_array($res);
								$a=$row['a'];													
						 ?>
				        <div class="form-group">
				          <label for="adNo">Admission Number:<span class="valid">*</span></label>
				            <input type="text" class="form-control2" name="adNo" id="adNo" value="<?php if($a==0 || $a=="" || $a==null)
																										 {
																										 echo "LA".'1000';
																										 } 
																										 else
																										 { 
																										 $next=$a+1;
																										 echo  "LA"."$next";
																										 }?>" readonly required>	
				        </div>
				        <div class="form-group">
				          <label for="dob">Date of Birth:</label>
				          <input type="text" name="dob" id="dob" class="form-control2 datepicker" >
				        </div>	
				         <div class="form-group">
				          <label for="gender">Gender:<span class="valid">*</span></label><BR />
				          <input type="radio" name="gender" id="male" value="male" checked="checked" class="form-control1">Male
						  <input type="radio" name="gender" id="female" value="female" class="form-control1">Female
				        </div>
				        <div class="form-group">
				          <label for="building">Building:</label>
				          <input type="text" name="building" id="building" class="form-control2" >			  
				        </div>
				        <div class="form-group">
				          <label for="street">Street:</label>
				          <input type="text" name="street" id="street" class="form-control2" >			  
				        </div>
				        <div class="form-group">
				          <label for="zone">Zone:</label>
				          <input type="text" name="zone" id="zone" class="form-control2" >			  
				        </div>	
					
					     <div class="form-group">
				          <label for="mobile">Mobile:<span class="valid">*</span></label>
				          <input type="text" name="mobile" id="mobile" class="form-control2" required>			  
				        </div>
				        <div class="form-group">
				          <label for="email">Email: </label>
				          <input type="text" name="email" id="email" class="form-control2" onfocus="clearbox('e')" >	
						   <div id="e" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>		  
				        </div> 
				         <div class="form-group">
				          <label for="nationality">Nationality:</label>
				          <input type="text" name="nationality" id="nationality" class="form-control2">
				        </div>
				        <div class="form-group">
				          <label for="idcardNo">ID/Passport: </label>
				          <input type="text" name="idcardNo" id="idcardNo" class="form-control2">					  
				        </div>
				        <div class="form-group">
				          <label for="photo">Photo: </label>
				          <input type="file" name="photo" id="photo" class="form-control2">		  
				        </div>  
				         <div class="form-group">
				          <label for="language">Language: </label>
				          <input type="text" name="language" id="language" class="form-control2">			  
				        </div>                    
				         
				    </div>
					<div class="col-sm-6">	                                       
				        <div class="form-group">
				          <label for="subjectId">Course:<span class="valid">*</span></label>
				          <select name="subjectId" id="subjectId" class="form-control2" required  onchange="getFee(this.value)">
								<option value="">Select</option>
								<?php 
								$select2="select * from ".TABLE_SUBJECT." order by subjectName";
								$res2=mysql_query($select2);
								while($row2=mysql_fetch_array($res2))
								{
								?>
								<option value="<?php echo $row2['ID']?>"><?php echo $row2['subjectName']." - ".$row2['place']." - ".$row2['countType'];?></option>
								<?php									
								}
								?>				
							</select>
				        </div>
				        <div class="form-group">
				          <label for="courseDetails">Course Details</label>
				          <textarea name="courseDetails" id="courseDetails" class="form-control2" style="height: 100%">	</textarea>	  
				        </div>                                                                              
				     <div>
				       <label for="school" style="color: #1c03fc">Academic Details</label>                     
				     </div>  
				        <div class="form-group">
				          <label for="school">School Name: </label>
				          <input type="text" name="school" id="school" class="form-control2">				  
				        </div>
				      
				        <div class="form-group">
				          <label for="classGrade">Class Grade: </label>
				          <input type="text" name="classGrade" id="classGrade" class="form-control2">				  
				        </div>	
				                                                         				                  
				    <div>
				      <label for="father" style="color: #1c03fc">Guardian Details</label>                     
				    </div>            								                   					
				        <div class="form-group">
				          <label for="father">Father Name:</label>
				          <input type="text" name="father" id="father" class="form-control2" >
				        </div>
				        <div class="form-group">
				          <label for="family">Family Name:</label>
				          <input type="text" name="family" id="family" class="form-control2" >
				        </div>
				        <div class="form-groupy">
				          <label for="fatherMobile">Father Mobile:</label>
				          <input type="text" name="fatherMobile" id="fatherMobile" class="form-control2" >				  
				        </div>
				        <div class="form-group">
				          <label for="fatherEmail">Father Email: </label>
				          <input type="text" name="fatherEmail" id="fatherEmail" class="form-control2" onfocus="clearbox('e2')">	
						  <div id="e2" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>			  
				        </div>
				        <div class="form-group">
				          <label for="hearAbout">Hear About: </label>
				          <input type="text" name="hearAbout" id="hearAbout" class="form-control2">				  
				        </div>
				        <div class="form-group">
				          <label for="referalName">Referal Name: </label>
				          <input type="text" name="referalName" id="referalName" class="form-control2">					  
				        </div>
				        <div class="form-group">
				          <label for="referalMobile">Referal Mobile: </label>
				          <input type="text" name="referalMobile" id="referalMobile" class="form-control2">				  
				        </div>
				        <div class="form-group">
				          <label for="other">Other: </label>
				          <input type="text" name="other" id="other" class="form-control2" >					  
				        </div>	
				        
				       </div> 
				     
				    </div>
				  
				  <div>
				</div>
				<div class="modal-footer">
				  <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
				</div>
				</form>
  			</div>
		</div>      
       </div>
	 </div>
   <!-- Modal1 cls -->
   
   <!-- Modal1  for enquiry form-->
      <div class="modal fade" id="myModalEnquiry" tabindex="-1" role="dialog">
        <div class="modal-dialog" id="enquiryDiv">

       </div>
	 </div>
   <!-- Modal1 cls -->
   
  
    <!-- myModalstudent  for enquiry form-->
     <div class="modal fade" id="myModalstudent" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
          	<div role="tabpanel" class="tabarea2">
	          <ul class="nav nav-tabs" role="tablist">
		            <li role="presentation" class="active"> <a href="myModalstudent" aria-controls="personal" role="tab" data-toggle="tab">STUDETS DETAILS</a> </li>
		      </ul>
		      <!-- Tab panes -->
			  <div class="tab-content" id="studentDiv">
			  </div>
			  <!-- Tab panes -->
	        </div>
         </div>
	   </div>
     </div>
   <!-- Modal1 cls -->
 <?php include("CalenderFooter.php") ?>
 