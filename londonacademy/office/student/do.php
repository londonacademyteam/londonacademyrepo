<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=$success=0;				
				$name			=	ucwords(strtolower($App->convert($_REQUEST['name'])));
				$idcardNo		=	$_REQUEST['idcardNo'];
				//next auto increment admission number	
					$res=mysql_query("select MAX(adNo) as a from ".TABLE_STUDENT."");
			  		$row=mysql_fetch_array($res);
					$a=$row['a'];
						if($a==0 || $a=="" || $a==null)
						 {
						 $nextAdNo='1000';
						 } 
						 else
						 { 
						 $nextAdNo=$a+1;						
						 }	
				
				$existId=$db->existValuesId(TABLE_STUDENT,"name='$name' AND idcardNo='$idcardNo'");
				if($existId>0)
				{
				$_SESSION['msg']="Student Is Already Exist";					
				header("location:new.php");					
				}
				else
				{				
				$data['name']			=	$name;
				$data['adNo']			=	$App->convert($nextAdNo);
				$data['father']			=	$App->convert($_REQUEST['father']);
				$data['family']			=	$App->convert($_REQUEST['family']);
				$data['dob']			=	$App->convert($_REQUEST['dob']);
				$data['gender']			=	$App->convert($_REQUEST['gender']);
				$data['building']		=	$App->convert($_REQUEST['building']);
				$data['street']			=	$App->convert($_REQUEST['street']);
				$data['zone']			=	$App->convert($_REQUEST['zone']);
				$data['nationality']	=	$App->convert($_REQUEST['nationality']);
				$data['idcardNo']		=	$App->convert($_REQUEST['idcardNo']);
				$data['mobile']			=	$App->convert($_REQUEST['mobile']);
				$data['fatherMobile']	=	$App->convert($_REQUEST['fatherMobile']);
				$data['email']			=	$App->convert($_REQUEST['email']);				
				$data['fatherEmail']	=	$App->convert($_REQUEST['fatherEmail']);
				$data['school']			=	$App->convert($_REQUEST['school']);
				$data['classGrade']		=	$App->convert($_REQUEST['classGrade']);
				$data['hearAbout']		=	$App->convert($_REQUEST['hearAbout']);
				$data['referalName']	=	$App->convert($_REQUEST['referalName']);
				$data['referalMobile']	=	$App->convert($_REQUEST['referalMobile']);
				$data['other']			=	$App->convert($_REQUEST['other']);
				$data['language']		=	$App->convert($_REQUEST['language']);
				$data['courseDetails']	=	$App->convert($_REQUEST['courseDetails']);	
					
				//***********************************************
				// upload file			
				$newName =$nextAdNo."_".$_FILES["photo"]["name"];
				$img='';	
				if(move_uploaded_file($_FILES["photo"]["tmp_name"],"uploads/".basename($newName)))
					{
						$img="uploads/" . $newName;		
					}
							
				$data['photo']			=	$App->convert($img);								
				//***********************************************
				//color ASSIGNMENT
				$num3	=	1;
				while($num3!=0)
				{
					$colr		=	'#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
					$select3	=	mysql_query("select * from ".TABLE_STAFF." where staffColor='$colr'");
					$num3		=	mysql_num_rows($select3);
				}	
				
				$data['studColor']		=	$App->convert($colr);				
				$success	=	$db->query_insert(TABLE_STUDENT,$data);
				
				$data2['studentId']	=	$success;	
				$data2['subjectId']	=	$App->convert($_REQUEST['subjectId']);				
				$success2	=	$db->query_insert(TABLE_STUDENT_SUBJECT,$data2);	
															
				if($success)
				{
					$joinId		=	$_REQUEST['joinId'];
				
					if($joinId!='0')
					{
						$data2['status']	=	'Joined';	
						$db->query_update(TABLE_ENQUIRY,$data2,"ID='{$joinId}'");								
						$db->close();	
					}
					$_SESSION['msg']="Student Basic Details Added Successfully";		
				}
								
				header("location:new.php");							
				}
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
			    	
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$name			=	ucwords(strtolower($App->convert($_REQUEST['name'])));
				$idcardNo		=	$_REQUEST['idcardNo'];	
				
				$res=mysql_query("select adNo from ".TABLE_STUDENT." where ID='$editId'");
			  	$row=mysql_fetch_array($res);
				$adNo=$row['adNo'];
										
				
				$existId=$db->existValuesId(TABLE_STUDENT,"name='$name' AND idcardNo='$idcardNo' AND ID!='$editId'");
				if($existId>0)
				{
				$_SESSION['msg']="Student Is Already Exist";					
				header("location:edit.php?id=$editId");					
				}
				else
				{				
				$data['name']			=	$name;
				$data['adNo']			=	$App->convert($adNo);
				$data['father']			=	$App->convert($_REQUEST['father']);
				$data['family']			=	$App->convert($_REQUEST['family']);
				$data['dob']			=	$App->convert($_REQUEST['dob']);
				$data['gender']			=	$App->convert($_REQUEST['gender']);
				$data['building']		=	$App->convert($_REQUEST['building']);
				$data['street']			=	$App->convert($_REQUEST['street']);
				$data['zone']			=	$App->convert($_REQUEST['zone']);
				$data['nationality']	=	$App->convert($_REQUEST['nationality']);
				$data['idcardNo']		=	$App->convert($_REQUEST['idcardNo']);
				$data['mobile']			=	$App->convert($_REQUEST['mobile']);
				$data['fatherMobile']	=	$App->convert($_REQUEST['fatherMobile']);
				$data['email']			=	$App->convert($_REQUEST['email']);				
				$data['fatherEmail']	=	$App->convert($_REQUEST['fatherEmail']);
				$data['school']			=	$App->convert($_REQUEST['school']);
				$data['classGrade']		=	$App->convert($_REQUEST['classGrade']);
				$data['hearAbout']		=	$App->convert($_REQUEST['hearAbout']);
				$data['referalName']	=	$App->convert($_REQUEST['referalName']);
				$data['referalMobile']	=	$App->convert($_REQUEST['referalMobile']);
				$data['other']			=	$App->convert($_REQUEST['other']);
				$data['language']		=	$App->convert($_REQUEST['language']);
				$data['courseDetails']	=	$App->convert($_REQUEST['courseDetails']);	
				$changePhoto			=	$_REQUEST['change'];
				if($changePhoto=='yes')
				{
					
					$res=mysql_query("select photo from ".TABLE_STUDENT." where ID='$editId'");
					$row=mysql_fetch_array($res);
					$photo=$row['photo'];
																				
					if (file_exists($photo)) 	
					{
					unlink($photo);					
					} 	
					
					$newName =$adNo."_".$_FILES["photo"]["name"];	
					if(move_uploaded_file($_FILES["photo"]["tmp_name"],"uploads/".basename($newName)))
					{
						$img="uploads/" . $newName;		
					}
				
				$data['photo']			=	$App->convert($img);
				}
				
				$db->query_update(TABLE_STUDENT,$data,"ID='{$editId}'");								
				$db->close();
				
				$_SESSION['msg']="Student Details Updated Successfully";					
				header("location:new.php");						
				}
			}		
		break;		
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				
					@mysql_query("DELETE FROM `".TABLE_FEEPAYMENT."` WHERE studentId='{$deleteId}'");	
					@mysql_query("DELETE FROM `".TABLE_STUDENT_SCHEDULE."` WHERE studentId='{$deleteId}'");	
					@mysql_query("DELETE FROM `".TABLE_STUDENT_SUBJECT."` WHERE studentId='{$deleteId}'");							
									
					$res=mysql_query("select photo from ".TABLE_STUDENT." where ID='{$deleteId}'");
					$row=mysql_fetch_array($res);
					$photo=$row['photo'];				
					$success= @mysql_query("DELETE FROM `".TABLE_STUDENT."` WHERE ID='{$deleteId}'");
					if($success)					
					{																																	
						if (file_exists($photo)) 	
						{
						unlink($photo);					
						} 						
					}									      
																			
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Student details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:delete.php");						
		break;	
	// Subject reg SECTION
	case 'sub':
		
		if(!$_REQUEST['studentId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;			
				
				$studentId		=	$App->convert($_REQUEST['studentId']);
				$subjectId		=	$App->convert($_REQUEST['subjectId']);
				$existId=$db->existValuesId(TABLE_STUDENT_SUBJECT,"studentId='$studentId' and subjectId='$subjectId'");
					if($existId>0)
					{
					$_SESSION['msg']="Subject is already allocated";					
					header("location:new.php");					
					}
					else
					{
					
					$data['studentId']			=	$App->convert($_REQUEST['studentId']);
					$data['subjectId']			=	$App->convert($_REQUEST['subjectId']);
					$success=$db->query_insert(TABLE_STUDENT_SUBJECT,$data);								
					$db->close();
					
						if($success)
						{
						$_SESSION['msg']="Subject allocated Successfully";											
						}	
						else
						{
						$_SESSION['msg']="Failed";											
						}
						header("location:new.php");	
					}									
				}						
		break;
		//SUBJECT DELETE SECTION
	case 'subDel':		
				$deleteId	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();								
				
				try
				{						
					$success= @mysql_query("DELETE FROM `".TABLE_STUDENT_SUBJECT."` WHERE ID='{$deleteId}'");												     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Subject details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");						
		break;				
}
?>