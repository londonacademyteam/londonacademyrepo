<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				/*//next auto increment staff number	
					$res=mysql_query("select MAX(staffId) as a from ".TABLE_STAFF."");
			  		$row=mysql_fetch_array($res);
					$a=$row['a'];	
						if($a==0 || $a=="" || $a==null)
						 {
						 $nextStaffId='1000';
						 } 
						 else
						 { 
						 $nextStaffId=$a+1;						
						 }	*/
				
				$user		=	$App->convert($_REQUEST['userName']);
				$userName	=	mysql_real_escape_string(htmlentities($user));
								
				$select		=	mysql_query("select * from ".TABLE_STAFF." where userName='$userName'");
				$num		=	mysql_num_rows($select);
				
				$select2	=	mysql_query("select * from ".TABLE_LOGIN." where userName='$userName'");
				$num2		=	mysql_num_rows($select2);
				
				$num3		=	1;
				while($num3!=0)
				{
					$colr		=	'#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
					$select3	=	mysql_query("select * from ".TABLE_STAFF." where staffColor='$colr'");
					$num3		=	mysql_num_rows($select3);
				}

				if($num!=0 || $num2!=0)
				{
					$_SESSION['msg']="User name already exist!";					
					header("location:new.php");		
				}
				else
				{
				$data['name']			=	ucwords(strtolower($App->convert($_REQUEST['name'])));	
				//$data['name']			=	$App->convert($_REQUEST['name']);
				$data['staffId']		=	$App->convert($_REQUEST['staffId']);
				$data['dob']			=	$App->convert($_REQUEST['dob']);
				$data['age']			=	$App->convert($_REQUEST['age']);
				$data['gender']			=	$App->convert($_REQUEST['gender']);
				$data['address']		=	$App->convert($_REQUEST['address']);
				$data['phone']			=	$App->convert($_REQUEST['phone']);
				$data['email']			=	$App->convert($_REQUEST['email']);
				$data['dateOfJoin']		=	$App->convert($_REQUEST['dateOfJoin']);
				$data['designation']	=	$App->convert($_REQUEST['designation']);
				$data['loginType']		=	$App->convert($_REQUEST['loginType']);
				$data['userName']		=	$App->convert($userName);
				$data['staffColor']		=	$colr;
				
				$password1				=	$App->convert($_REQUEST['password']);
						
				$password2				=	mysql_real_escape_string(htmlentities($password1));				
				$password3				=	md5($password2); // Encrepted Password
				$password4				=	mysql_real_escape_string(htmlentities($password3)); 					
				$data['password']		=	$App->convert($password4);					
				
				
				$s=$db->query_insert(TABLE_STAFF,$data);								
				$db->close();
					if($s)
					{
					$email		=	$App->convert($_REQUEST['email']);
					$userName	=	$App->convert($userName);
					$password	=	$App->convert($password2);
					$subject	=	"London Academy";
					$message	=	"UserName=".$userName."</br>"."Password=".$password;
					mail($email,$subject,$message);
					
					$_SESSION['msg']="Staff Basic Details Added Successfully";					
					header("location:new.php");	
					}	
				}				
				
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
			    	
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$res=mysql_query("select staffId from ".TABLE_STAFF." where ID='$editId'");
			  	$row=mysql_fetch_array($res);
				$staffId=$row['staffId'];
									
				
				$user			=	$App->convert($_REQUEST['userName']);
				$userName		=	mysql_real_escape_string(htmlentities($user));
				
				$select			=	mysql_query("select * from ".TABLE_STAFF." where userName='$userName' and ID!='$editId'");
				$num			=	mysql_num_rows($select);
			
				$select2		=	mysql_query("select * from ".TABLE_LOGIN." where userName='$userName'");
				$num2			=	mysql_num_rows($select2);
				
				if($num!=0 || $num2!=0)
				{
					$_SESSION['msg']="User name already exist!";					
					header("location:edit.php?id=$editId");		
				}
				else
				{
				
				$data['name']		=	ucwords(strtolower($App->convert($_REQUEST['name'])));	
				//$data['name']			=	$App->convert($_REQUEST['name']);
				$data['staffId']		=	$App->convert($staffId);
				$data['dob']			=	$App->convert($_REQUEST['dob']);
				$data['age']			=	$App->convert($_REQUEST['age']);
				$data['gender']			=	$App->convert($_REQUEST['gender']);
				$data['address']		=	$App->convert($_REQUEST['address']);
				$data['phone']			=	$App->convert($_REQUEST['phone']);
				$data['email']			=	$App->convert($_REQUEST['email']);
				$data['dateOfJoin']		=	$App->convert($_REQUEST['dateOfJoin']);	
				$data['designation']	=	$App->convert($_REQUEST['designation']);
				$data['loginType']		=	$App->convert($_REQUEST['loginType']);				
				$data['userName']		=	$App->convert($userName);
				
				$password1				=	$App->convert($_REQUEST['password']);
						
				$password2				=	mysql_real_escape_string(htmlentities($password1));				
				$password3				=	md5($password2); // Encrepted Password
				$password4				=	mysql_real_escape_string(htmlentities($password3)); 					
				$data['password']		=	$App->convert($password4);								
								
				
				
				$s=$db->query_update(TABLE_STAFF,$data,"ID='{$editId}'");								
				$db->close();
					if($s)
					{
					$email		=	$App->convert($_REQUEST['email']);
					$userName	=	$App->convert($userName);
					$password	=	$App->convert($password2);
					$subject	=	"London Academy";
					$message	=	"UserName=".$userName."</br>"."Password=".$password;
					mail($email,$subject,$message);
					
					$_SESSION['msg']="Staff Details Updated Successfully";					
					header("location:new.php");	
					}
				}					
				
			}		
		break;		
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_STAFF."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Staff details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");					
		break;		
// Subject reg SECTION
	case 'sub':
		
		if(!$_REQUEST['staffId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;			
				
				$staffId			=	$App->convert($_REQUEST['staffId']);
				$subjectName		=	$App->convert($_REQUEST['subjectName']);
				$existId=$db->existValuesId(TABLE_STAFF_SUBJECT,"staffId='$staffId' and subjectName='$subjectName'");
					if($existId>0)
					{
					$_SESSION['msg']="Subject is already allocated";					
					header("location:new.php");					
					}
					else
					{
					
					$data['staffId']			=	$App->convert($_REQUEST['staffId']);
					$data['subjectName']		=	$App->convert($_REQUEST['subjectName']);
					$success=$db->query_insert(TABLE_STAFF_SUBJECT,$data);								
					$db->close();
					
						if($success)
						{
						$_SESSION['msg']="Subject allocated Successfully";											
						}	
						else
						{
						$_SESSION['msg']="Failed";											
						}
						header("location:new.php");	
					}									
				}						
		break;
//SUBJECT DELETE SECTION
	case 'subDel':		
				$deleteId	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();								
				
				try
				{						
					$success= @mysql_query("DELETE FROM `".TABLE_STAFF_SUBJECT."` WHERE ID='{$deleteId}'");												     
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Subject details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");						
		break;				
}
?>