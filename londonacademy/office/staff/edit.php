<?php include("../adminHeader.php") ?>

<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

</script>
<script>
//calc age
function getAge()
{
var dob=document.getElementById('dob').value;

//today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = dd+'-'+mm+'-'+yyyy;

//today end
var dobSplit= dob.split('/');
var todaySplit= today.split('-');

var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);

var age=Math.round(dateDiff/365);
if(age<=0)
{
alert("Enter a valid date of birth");
}
else
{
document.getElementById('age').value=age;
}
}

</script>

<script>
function valid()
{
flag=false;
	email=document.getElementById('email').value;		
		reguler2 = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;
		if(!email.match(reguler2))
		{
		document.getElementById('e').innerHTML="Invalid Email address";
		flag=true;
		}
		
	if(flag==true)
	{
	return false;
	}
	
																				
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

		$editId=$_REQUEST['id'];
	$tableEdit=mysql_query("SELECT * FROM `".TABLE_STAFF."` WHERE ID='$editId'");	
	$editRow=mysql_fetch_array($tableEdit);
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">STAFF REGISTRATION </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>">
                <div class="row">
                  <div class="col-sm-6">
                  	<div class="form-group">
						<label for="staffId">StaffID:<span class="valid">*</span></label>					
                  		<input type="text" name="staffId" id="staffId" value="<?php echo $editRow['staffId']?>" class="form-control2" required>
					</div>
					
                     <div class="form-group">
                      <label for="name">Name:<span class="valid">*</span></label>
                      <input type="text" class="form-control2" name="name" id="name" required value="<?php echo $editRow['name']?>">
                    </div>
					<div class="form-group">
						<label for="address">Address:<span class="valid">*</span></label>
						<textarea name="address" id="address" class="form-control2" required style="height:70%"><?php echo $editRow['address'] ?></textarea>
					</div>
                    <div class="form-group">
						<label for="phone">Phone:</label>	
						<input type="text" name="phone" id="phone" class="form-control2" value="<?php echo $editRow['phone'] ?>" required>
					</div>
					
					
					<div class="form-group">
						<label for="dob">Date of Birth:</label>
						<input type="text" name="dob" id="dob" value="<?php echo $editRow['dob'] ?>" class="form-control2 datepicker" readonly>
					</div>
                    <div class="form-group">
						<label for="age">Age:</label>
						<input type="text" name="age" id="age" onClick="getAge()" value="<?php echo $editRow['age'] ?>"  readonly="readonly" class="form-control2">
					</div>															
				</div>
				
				
				
				<div class="col-sm-6"></br>
                   <div class="form-group">
					<label for="gender" >Gender:</label></br >
			<input type="radio" name="gender" id="male" value="male" <?php if($editRow['gender']=='male'){?> checked="checked"<?php }?>>Male
			<input type="radio" name="gender" id="female" value="female" <?php if($editRow['gender']=='female'){?> checked="checked"<?php }?>>Female
					</div>
					<div class="form-group">
						<label for="email">Email:<span class="valid">*</span></label>					
						<input type="text" name="email" id="email" class="form-control2" value="<?php echo $editRow['email'] ?>" onfocus="clearbox('e')" required> 
						<div id="e" style="color:#FF6600; font-family:'Times New Roman', Times, serif"></div>
					</div>
					
                      <div class="form-group">
						<label for="designation">Designation:</label>
						<select name="designation" id="designation" class="form-control2" required>
						<option value="">Select</option>
                        <?php								
						$select3=mysql_query("select * 
												from ".TABLE_DESIGNATIONS." 
												order by designation");
															
						  //$num3=mysql_num_rows($select3);
						  while($subRow=mysql_fetch_array($select3))
						  {							  	
						  ?>
						  	
                        	<option value="<?php echo $subRow['ID'] ?>" <?php if($editRow['designation']==$subRow['ID']){?> selected<?php }?>><?php echo $subRow['designation'];?></option>
	                        <?php
							  }
							  ?>
                        </select>
					</div>						
					<div class="form-group">
						<label for="dateOfJoin">Date Of Join:</label>
					<input type="text" name="dateOfJoin" id="dateOfJoin" class="form-control2 datepicker" value="<?php echo $editRow['dateOfJoin'] ?>" readonly>
					</div>
					<div class="form-group">
						<label for="loginType">Login Type:</label>
						<select name="loginType" id="loginType" class="form-control2" required>
                        	<option value="teacher" <?php if($editRow['loginType']=='teacher'){ echo 'selected'; }?>>Teacher</option>
                         	<option value="officeStaff" <?php if($editRow['loginType']=='officeStaff'){ echo 'selected'; }?>>Office Staff</option>
                        </select>
					</div>						
					<div class="form-group">
						<label for="userName">UserName:<span class="valid">*</span></label>
						<input type="text" name="userName" id="userName" class="form-control2" value="<?php echo $editRow['userName'] ?>" required>
					</div>
					<div class="form-group">
						<label for="password">Password:<span class="valid">*</span></label>					
						<input type="password" name="password" id="password" class="form-control2" required>
					</div>						
				</div>
               </div>                  
             </div>              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>
