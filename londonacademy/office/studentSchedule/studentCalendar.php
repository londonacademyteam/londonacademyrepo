
<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId = $_SESSION['LogID'];
$loginType = $_SESSION['LogType'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
	
	$cond="1";
	if(@$_REQUEST['sname'])
	{	
	$a=$_REQUEST['sname'];
	$rest = substr("$a", 2);
	$cond=$cond." and ((".TABLE_STUDENT.".name like'%".$_POST['sname']."%') or (".TABLE_STUDENT.".adNo like'%$rest%') or (".TABLE_STUDENT.".mobile like'%".$_POST['sname']."%'))";
	}


$select=mysql_query("select ".TABLE_STUDENT_SCHEDULE.".classDate,
							".TABLE_STAFF.".name,
							".TABLE_STUDENT_SCHEDULE.".timeFrom,
							".TABLE_STUDENT_SCHEDULE.".timeTo,
							".TABLE_SUBJECT.".subjectName,
							".TABLE_STUDENT.".name as studName,
							".TABLE_STUDENT.".studColor
					from ".TABLE_STUDENT_SCHEDULE.",".TABLE_STAFF.",".TABLE_SUBJECT.",".TABLE_STUDENT."
					where ".TABLE_STUDENT_SCHEDULE.".teacherId=".TABLE_STAFF.".ID 
						and ".TABLE_STUDENT_SCHEDULE.".studentId=".TABLE_STUDENT.".ID
						and `".TABLE_STUDENT_SCHEDULE."`.subjectId=`".TABLE_SUBJECT."`.ID						
						and $cond");	

$results = array();
while($row=mysql_fetch_array($select))
		{
			$results[] = $row;
		}
$leng=mysql_num_rows($select);
 
?>
<link rel='stylesheet' href='agenda/lib/cupertino/jquery-ui.min.css' />
<link href='agenda/fullcalendar.css' rel='stylesheet' />
<link href='agenda/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='agenda/lib/moment.min.js'></script>
<script src='agenda/lib/jquery.min.js'></script>
<script src='agenda/fullcalendar.min.js'></script>
<script src='agenda/lang-all.js'></script>
<script>

	$(document).ready(function() {
		
		var currentLangCode = 'en';

		// build the language selector's options
		$.each($.fullCalendar.langs, function(langCode) {
			$('#lang-selector').append(
				$('<option/>')
					.attr('value', langCode)
					.prop('selected', langCode == currentLangCode)
					.text(langCode)
			);
		});

		// rerender the calendar when the selected option changes
		$('#lang-selector').on('change', function() {
			if (this.value) {
				currentLangCode = this.value;
				$('#calendar').fullCalendar('destroy');
				renderCalendar();
			}
		});
		
		function renderCalendar() {
		$('#calendar').fullCalendar({
			theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '<?php echo date("Y-m-d"); ?>',
			editable: false,
			lang: currentLangCode,
			eventLimit: true, // allow "more" link when too many events
			events: [
			<?php for($i=0;$i< $leng;$i++)
				{
					$color		=	$results[$i][6];
					$dateFrom	=	$results[$i][0];	
					$dateTo		=	$results[$i][0];
					$dateTo 	= 	date('Y-m-d', strtotime($dateTo .' +1 day'));	
			
					$begin = new DateTime($dateFrom);
					$end = new DateTime($dateTo);
	
					$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
					foreach($daterange as $date)
					{
						$dt[$i]=$date->format("Y-m-d");
						
							$startDate = $dt[$i].'T'.$results[$i][2];
							$endDate = $dt[$i].'T'.$results[$i][3];								
						?>
						{
							overlap: false, 			
							//title: "<?php echo $results[$i][1].'-'.$results[$i][5].'-'.$results[$i][4]; ?>",
							title: "<?php echo $results[$i][5].'-'.$results[$i][4]; ?>",
							start : '<?php echo $startDate; ?>',
							//start : '2015-12-22T10:30:00',
							end : '<?php echo $endDate; ?>',
							//end : '2015-12-23T11:30:00'	
							color : '<?php echo $color;?>'												
						},
						<?php
					}
				}
						?>
			]
		});
		
		}
		renderCalendar();
		
	});

</script>
<style>
	body {
		
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	
	#top {
		background: #eee;
		border-bottom: 1px solid #ddd;
		padding: 0 10px;
		line-height: 40px;
		font-size: 13px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}
	.tablearea3 td {
    padding: 18px 15px !important;}

</style>


<div class="col-md-10 col-sm-8 rightarea">
        <div class="student-shedule">
          <div class="row rightareatop">
            <div class="col-sm-8">
              <h2>STUDENT CALENDAR</h2>
            </div>
            <div class="col-sm-4">
	            <form method="post">
	              <div class="input-group">
				  <input type="text" class="form-control"  name="sname" placeholder="Student Name / Ad.No / Phone " value="<?php echo @$_REQUEST['sname'] ?>">             
	                <span class="input-group-btn">
	                <button class="btn btn-default lens" type="submit"></button>
	                </span> </div>
	            </form>
            </div>
 
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div style="">
        
		
		<!--    Calender   -->
						<div id='calendar'><div id='top'>
					
							Language:
							<select id='lang-selector'></select>
					
						</div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
     
    </div>
  </div>
    <?php include("CalenderFooter.php") ?>