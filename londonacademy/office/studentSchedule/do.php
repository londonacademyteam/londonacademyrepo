<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['studentId'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new2.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$studentId	=	$App->convert($_REQUEST['studentId']);
				$teacherId	=	$App->convert($_REQUEST['teacherId']);
				$subjectId	=	$App->convert($_REQUEST['subjectId']);
				$classDate	=	$App->dbformat_date($_REQUEST['classDate']);
				$timeFrom	=	$_REQUEST['timeFromHr'].":".$_REQUEST['timeFromMin']." ".$_REQUEST['type1'];
				$timeTo		=	$_REQUEST['timeToHr'].":".$_REQUEST['timeToMin']." ".$_REQUEST['type2'];
				$timeFrom	=	$App->convert($timeFrom);
				$timeTo		=	$App->convert($timeTo);
				
				$timeFrom  = date("H:i:s", strtotime($timeFrom));
				$timeTo    = date("H:i:s", strtotime($timeTo));
				
				
				$existId=$db->existValuesId(TABLE_STUDENT_SCHEDULE,"studentId='$studentId' 
																	AND classDate='$classDate' 
																	AND ((timeFrom='$timeFrom' && timeTo='$timeTo') 
																		OR (timeFrom>'$timeFrom' && timeTo>'$timeTo' && timeFrom<'$timeTo') 
																		OR (timeFrom<'$timeFrom' && timeTo>'$timeFrom' && timeTo<'$timeTo')
																		OR (timeFrom>'$timeFrom' && timeTo<'$timeTo') 
																		OR (timeFrom<'$timeFrom' && timeTo>'$timeTo'))");
				
																		
				$existId2=$db->existValuesId(TABLE_STUDENT_SCHEDULE,"teacherId='$teacherId' 
																	AND classDate='$classDate' 
																	AND subjectId!='$subjectId'
																	AND ((timeFrom='$timeFrom' && timeTo='$timeTo') 
																		OR (timeFrom>'$timeFrom' && timeTo>'$timeTo' && timeFrom<'$timeTo') 
																		OR (timeFrom<'$timeFrom' && timeTo>'$timeFrom' && timeTo<'$timeTo')
																		OR (timeFrom>'$timeFrom' && timeTo<'$timeTo') 
																		OR (timeFrom<'$timeFrom' && timeTo>'$timeTo'))");
				if($existId>0)
				{
				$_SESSION['msg']="Student already Scheduled";					
				header("location:new2.php?id=$studentId");					
				}
				else if($existId2>0)
				{
				$_SESSION['msg']="Teacher already scheduled for another subject";					
				header("location:new2.php?id=$studentId");					
				}
				else
				{							
				$data['studentId']		=	$studentId;							
				$data['subjectId']		=	$subjectId;
				$data['classDate']		=	$classDate;
				$data['timeFrom']		=	$timeFrom;
				$data['timeTo']			=	$timeTo;
				$data['teacherId']		=	$teacherId;				
				$data['amount']			=	$App->convert($_REQUEST['amount']);
								
				$success=$db->query_insert(TABLE_STUDENT_SCHEDULE,$data);								
				$db->close();
					if($success)
					{
						$_SESSION['msg']="Schedule Added successfully";										
					}	
					else
					{
						$_SESSION['msg']="Failed";										
					}	
				
				}				
				header("location:new2.php?id=$studentId");							
				
			}		
		break;
		
		
				
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$studentId	=	$App->convert($_REQUEST['sid']);				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
								
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_STUDENT_SCHEDULE."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Schedule deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new2.php?id=$studentId");
								
		break;	
}
?>