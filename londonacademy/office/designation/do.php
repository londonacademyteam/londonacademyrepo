<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['designation'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
				
				$data['designation']		=	ucwords(strtolower($App->convert($_REQUEST['designation'])));			
								
				$success	=	$db->query_insert(TABLE_DESIGNATIONS,$data);								
				$db->close();
				if($success)
				{
					$_SESSION['msg']="Designations Added Successfully";										
				}	
				else
				{
					$_SESSION['msg']="Failed";											
				}	
				header("location:new.php");									
			}		
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id']; 
		if(!$_REQUEST['designation'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
				$data['designation']		=	ucwords(strtolower($App->convert($_REQUEST['designation'])));
				$success	=	$db->query_update(TABLE_DESIGNATIONS,$data,"ID='{$editId}'");								
				$db->close();
				if($success)
				{
					$_SESSION['msg']="Designation Details updated Successfully";										
				}	
				else
				{
					$_SESSION['msg']="Failed";											
				}	
				header("location:new.php");									
			}								
		break;		
	// DELETE SECTION	
		case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_DESIGNATIONS."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Designation details deleted successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:new.php");		
		break;			
}
?>