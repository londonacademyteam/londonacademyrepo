<?php include("../adminHeader.php") ;
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
$loginId=$_SESSION['LogID'];
$loginType=$_SESSION['LogType'];
?>

<div class="col-md-10 col-sm-8 rightarea">
	<div class="discussionpage">
	  <div class=" rightareatop">
	    <h2 class="discussion-hd">ALERT</h2>
	  </div>
<?php
if(isset($_REQUEST['id']))
{
	$tableId=$_REQUEST['id'];
	$selAllQuery2="select ".TABLE_ENQUIRY.".ID,
							".TABLE_ENQUIRY.".enquiryDate,
							".TABLE_ENQUIRY.".studentName,
							".TABLE_ENQUIRY.".studClass,
							".TABLE_SUBJECT.".subjectName,
							".TABLE_ENQUIRY.".phone,
							".TABLE_ENQUIRY.".school,
							".TABLE_ENQUIRY.".location,
							".TABLE_ENQUIRY.".feeDetails,
							".TABLE_ENQUIRY.".remark,
							".TABLE_ENQUIRY.".parentName,
							".TABLE_ENQUIRY.".status,
							".TABLE_ENQUIRY.".alertDate
						from `".TABLE_ENQUIRY."`,".TABLE_SUBJECT."
						where ".TABLE_SUBJECT.".ID=".TABLE_ENQUIRY.".subjectId 
							and ".TABLE_ENQUIRY.".ID='$tableId' 
							and ".TABLE_ENQUIRY.".status='Pending' 
						order by ".TABLE_ENQUIRY.".ID desc";				
	$selectAll2= $db->query($selAllQuery2);
	$row=mysql_fetch_array($selectAll2)	;
?>
 <div class="discussioncontent">
    <div class="discussioncontent1">
    	<div class="tab-pane"> <span style="float:left;"></div>
          <div style="width:90%; margin-left:30px">
            <p>
            	<table class="table nobg no_brdr" id="headerTable2" >
                    <tbody style="background-color:#FFFFFF">
                      <tr>
                        <td>Enquiry Date</td>
                        <td>:</td>
                        <td><?php echo $App->dbFormat_date($row['enquiryDate']); ?></td>
                      </tr>
                      <tr>
                        <td>Student Name</td>
                        <td>:</td>
                        <td><?php echo $row['studentName']; ?></td>
                      </tr>
                      <tr>
                        <td>Class</td>
                        <td>:</td>
                        <td><?php echo $row['studClass']; ?></td>
                      </tr>
                      <tr>
                        <td>Subject</td>
                        <td>:</td>
                        <td><?php echo $row['subjectName']; ?></td>
                      </tr>
                      
                      <tr>
                        <td>Land Phone</td>
                        <td>:</td>
                        <td><?php echo $row['phone']; ?></td>
                      </tr>
                      <tr>
                        <td>School</td>
                        <td>:</td>
                        <td><?php echo $row['school']; ?></td>
                      </tr>
                      <tr>
                        <td>Location</td>
                        <td>:</td>
                        <td><?php echo $row['location']; ?></td>
                      </tr>
                      <tr>
                        <td>Fee</td>
                        <td>:</td>
                        <td><?php echo $row['feeDetails']; ?></td>
                      </tr>
                      <tr>
                        <td>Remark</td>
                        <td>:</td>
                        <td><?php echo $row['remark']; ?></td>
                      </tr>
                      <tr>
                        <td>Parent Name</td>
                        <td>:</td>
                        <td><?php echo $row['parentName']; ?></td>
                      </tr>
                      <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td><?php echo $row['status']; ?></td>
                      </tr>
                      <tr>
                        <td>Alert Date</td>
                        <td>:</td>
                        <td><?php echo $App->dbFormat_date($row['alertDate']); ?></td>
                      </tr>
                      
                    </tbody>
                  </table>
                </p>
          </div>
        </div>
      </div>

<?php
}
else
{
	$today=date('Y-m-d');
	$selAllQuery="select studentName,alertDate,ID 
					  						from `".TABLE_ENQUIRY."` 
					  						where alertDate>='$today'
					  							and status='Pending' 
					  						order by alertDate";
					
	$selectAll= $db->query($selAllQuery);	
	$number=mysql_num_rows($selectAll);
?>

<?php if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }$_SESSION['msg']='';?>
<?php 
					if($number==0)	
					{
					?>
                  <h6 align="center">No data found</h6>
                  <?php
					}
					else
					{	
						$flag=0;
					?>
                    <div class="discussioncontent">
                    <div class="discussioncontent1">
                      <?php	
                            while($row=mysql_fetch_array($selectAll))
                            {
                                $tableId		=	$row['ID'];
                                $studentName	=	$row['studentName'];
                                $alertDate		=	$row['alertDate'];								
                                $date1			=	date_create($alertDate);
                                $today			=	date('Y-m-d');
                                $date2			=	date_create($today);
                                $diff			=	date_diff($date2,$date1);
                                $dateDiff		=	$diff->format("%a");
                                $dateSign		=	$diff->format("%R");
                                if($dateSign=='+')
                                {                                   									
                      ?>
                      <div class="notification_block"> <span style="float:left;"> <i class="fa fa-sticky-note"></i> </span> </div>
                      <div style="margin-left:30px">
                        <p><?php echo "Follow up is needed for $studentName on ".$App->dbformat_date($alertDate); ?></p>
                      </div>
                      <?php	
                                }																		                             	
                            }
                       ?>
			    </div>
			  </div>
			<?php                      
					}
					?>   
<?php
}
?>
 </div>
  </div>
  

  <?php include("../adminFooter.php") ?>