-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 05, 2017 at 06:32 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_london`
--

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `cv` varchar(50) NOT NULL,
  `vacancyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `ID` int(11) NOT NULL,
  `designation` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`ID`, `designation`) VALUES
(1, 'Faculty'),
(5, 'Centre Manager'),
(4, 'Office Assistant'),
(6, 'Admin'),
(7, 'Student Counselor'),
(8, 'Driver'),
(9, 'Director'),
(10, 'Chairman');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `ID` int(11) NOT NULL,
  `enquiryDate` date NOT NULL,
  `studentName` varchar(100) NOT NULL,
  `studClass` varchar(20) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `school` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `building` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `zone` varchar(50) NOT NULL,
  `feeDetails` int(50) NOT NULL,
  `hourNeeded` int(11) NOT NULL,
  `remark` text NOT NULL,
  `parentName` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `alertDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`ID`, `enquiryDate`, `studentName`, `studClass`, `subjectId`, `phone`, `school`, `location`, `building`, `street`, `zone`, `feeDetails`, `hourNeeded`, `remark`, `parentName`, `status`, `alertDate`) VALUES
(1, '2016-01-23', 'Khalid', '10', 34, '55546788', '', '', '', '', '', 250, 0, 'He will come at 1pm to take classes at centre on 24th Jan 2015.', '', 'Pending', '2016-01-24'),
(2, '2016-01-22', 'Ayisha & Noof', '1,2', 14, '', '', '', '', '', '', 250, 0, 'Customer don\'t want to give more details. Customer will call back.', '', 'Pending', '2016-01-24'),
(3, '2016-01-22', 'Mohamad', '8,7,6,4', 59, '66503931', '', '', '', '', '', 150, 0, 'Previous customer, WIll check and call back', '', 'Pending', '2016-01-24'),
(4, '2016-01-23', 'Salama', '8', 8, '66057010', '', '', '', '', '', 150, 0, 'She will come to the centre on 24th Jan', '', 'Pending', '2016-01-24'),
(5, '2016-01-22', 'Jawahara', '6', 1, '55557670', '', '', '', '', '', 100, 0, 'Class Scheduled. She will come to the centre on 23rd Jan', '', 'Pending', '2016-01-23'),
(6, '2016-01-22', 'Rowda', '2', 9, '55557670', '', '', '', '', '', 150, 0, '', '', 'Pending', '2016-01-23'),
(7, '2016-02-04', 'Franklin', '12th', 50, '424380984', 'Lonodn Academy', 'banihajer', '12', 'year', 'ho88', 250, 12, '', 'Hamed', 'Pending', '2016-02-22');

-- --------------------------------------------------------

--
-- Table structure for table `feepayment`
--

CREATE TABLE `feepayment` (
  `ID` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `paidAmount` int(11) NOT NULL,
  `paymentDate` date NOT NULL,
  `discount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feepayment`
--

INSERT INTO `feepayment` (`ID`, `studentId`, `paidAmount`, `paymentDate`, `discount`) VALUES
(1, 1, 300, '2016-01-23', 0),
(2, 1, 150, '2016-01-23', 0),
(3, 2, 150, '2016-01-23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `group_details`
--

CREATE TABLE `group_details` (
  `ID` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `studentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_details`
--

INSERT INTO `group_details` (`ID`, `groupId`, `studentId`) VALUES
(1, 2, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `ID` int(11) NOT NULL,
  `staffId` int(11) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `staffId`, `userName`, `password`, `type`) VALUES
(1, 0, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `ID` int(11) NOT NULL,
  `staffId` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `age` int(11) NOT NULL,
  `address` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `dateOfJoin` varchar(20) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `loginType` varchar(20) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `staffColor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`ID`, `staffId`, `name`, `gender`, `dob`, `age`, `address`, `phone`, `email`, `dateOfJoin`, `designation`, `loginType`, `userName`, `password`, `staffColor`) VALUES
(1, '1000', 'Suneer Aboobacker', 'male', '15/12/1982', 33, 'Valiyaveetil House,', '30205320', 'pa.suneer@gmail.com', '01/09/2015', '2', 'officeStaff', 'suneer', '0ae6d56a7266ffb8acec54193fa6854a', '#e13705'),
(3, '1001', 'Mohamad Omaira', 'male', '11/06/1955', 61, 'Cairo,Egypt', '30600496', 'info@londonacademyqatar.com', '25/02/2015', '1', 'teacher', 'mohamad omaira', '0ae6d56a7266ffb8acec54193fa6854a', '#de1046'),
(4, '1002', 'Malini Unni', 'female', '10/03/1972', 44, 'No: 140/65, South Ma', '77079239', 'info@londonacademyqatar.com', '30/03/2015', '1', 'teacher', 'malini unni', '0ae6d56a7266ffb8acec54193fa6854a', '#7f491c'),
(5, '1003', 'Subba Surendra', 'male', '12/01/1996', 20, 'Nepal', '70919349', 'info@londonacademyqatar.com', '25/08/2015', '2', 'officeStaff', 'subba surendra', '0ae6d56a7266ffb8acec54193fa6854a', '#9f2a54'),
(6, '1004', 'Naseem Banu', 'female', '19/05/1962', 54, 'Brindavan Avanue, Ko', '30205414', 'info@londonacademyqatar.com', '06/06/2015', '1', 'teacher', 'naseem banu', '0ae6d56a7266ffb8acec54193fa6854a', '#feb174'),
(7, '1005', 'Deepa Sajikumar', 'female', '11/09/1971', 44, '303, Radhakrishna To', '70356514', 'info@londonacademyqatar.com', '12/09/2015', '1', 'teacher', 'Deepa Sajikumar', '0ae6d56a7266ffb8acec54193fa6854a', '#0a0954'),
(8, '1006', 'Franklin Dasan Anton', 'male', '13/02/1981', 35, '316-50/5,Indra Nagar', '70356788', 'info@londonacademyqatar.com', '07/09/2015', '1', 'teacher', 'frnaklin dasan antony', '0ae6d56a7266ffb8acec54193fa6854a', '#18a934'),
(10, '1007', 'Abdul Nazar', 'male', '13/11/1964', 51, 'Vammadath House, Eda', '66421416', 'info@londonacademyqatar.com', '19/09/2015', '2', 'officeStaff', 'abdul nazar', '0ae6d56a7266ffb8acec54193fa6854a', '#a1e32f'),
(11, '1008', 'Rabab Mohamad', 'female', '20/05/1988', 28, 'Egypt', '66636093', 'info@londonacademyqatar.com', '05/11/2015', '1', 'teacher', 'rabab mohamad', '0ae6d56a7266ffb8acec54193fa6854a', '#91c0fe'),
(12, '1009', 'Bhagya Lakshmi', 'female', '02/07/1977', 39, 'No 25/13, 1st floor,', '70356338', 'info@londonacademyqatar.com', '14/11/2015', '1', 'teacher', 'bhagya lakshmi', '0ae6d56a7266ffb8acec54193fa6854a', '#bbf7a0'),
(13, '1010', 'Ashkar', 'male', '22/05/1985', 31, 'Chalikkandy House, T', '30975690', 'info@londonacademyqatar.com', '17/11/2015', '2', 'officeStaff', 'ashkar', '0ae6d56a7266ffb8acec54193fa6854a', '#5c658c'),
(14, 'LAQ0000', 'Abdulla', 'male', '', 0, 'India', '70246454', 'info@londonacademyqatar.com', '', '1', 'teacher', 'abdulla', '0ae6d56a7266ffb8acec54193fa6854a', '#2f06b2'),
(15, 'LAQ1001', 'Mansib Ibrahim', 'male', '', 0, 'London Academy, Qata', '33540739', 'director@londonacademyqatar.co', '', '9', 'teacher', 'mansib ibrahim', '0ae6d56a7266ffb8acec54193fa6854a', '#041675');

-- --------------------------------------------------------

--
-- Table structure for table `staff_subject`
--

CREATE TABLE `staff_subject` (
  `ID` int(11) NOT NULL,
  `staffId` int(11) NOT NULL,
  `subjectName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_subject`
--

INSERT INTO `staff_subject` (`ID`, `staffId`, `subjectName`) VALUES
(1, 1, 'Arabic'),
(2, 1, 'English'),
(3, 1, 'Maths'),
(4, 3, 'Arabic'),
(6, 4, 'Business Studies'),
(7, 4, 'Maths'),
(8, 4, 'Science'),
(9, 4, 'English'),
(10, 4, 'Travel & Tourism'),
(11, 4, 'Physics'),
(12, 4, 'Chemistry'),
(13, 4, 'Biology'),
(14, 3, 'English'),
(15, 3, 'Biology'),
(16, 3, 'Chemistry'),
(17, 3, 'Physics'),
(18, 3, 'Maths'),
(19, 3, 'Quran'),
(20, 3, 'Science'),
(21, 6, 'Biology'),
(22, 6, 'English'),
(23, 6, 'Science'),
(24, 7, 'Maths'),
(25, 7, 'Physics'),
(26, 7, 'Chemistry'),
(28, 8, 'Biology'),
(30, 8, 'Science'),
(34, 11, 'Arabic'),
(35, 11, 'Maths'),
(36, 11, 'Science'),
(37, 11, 'Physics'),
(38, 11, 'Chemistry'),
(39, 11, 'Biology'),
(40, 12, 'English'),
(41, 12, 'Ielts/toefl'),
(46, 6, 'All Subjects'),
(47, 4, 'All Subjects'),
(48, 12, 'All Subjects'),
(49, 12, 'Business Studies'),
(50, 7, 'All Subjects'),
(51, 7, 'Act/sat'),
(52, 8, 'All Subjects'),
(53, 7, 'Science'),
(54, 11, 'All Subjects'),
(55, 14, 'Act/sat'),
(56, 14, 'Maths'),
(57, 15, 'Maths'),
(58, 15, 'Act/sat'),
(59, 3, 'All Subjects');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `ID` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `adNo` int(11) NOT NULL,
  `father` varchar(20) NOT NULL,
  `family` text NOT NULL,
  `dob` varchar(20) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `building` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `zone` varchar(50) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `idcardNo` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `fatherMobile` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fatherEmail` varchar(50) NOT NULL,
  `school` varchar(50) NOT NULL,
  `classGrade` varchar(20) NOT NULL,
  `hearAbout` varchar(40) NOT NULL,
  `referalName` varchar(20) NOT NULL,
  `referalMobile` varchar(20) NOT NULL,
  `other` varchar(20) NOT NULL,
  `language` varchar(20) NOT NULL,
  `courseDetails` text NOT NULL,
  `photo` text NOT NULL,
  `studColor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ID`, `name`, `adNo`, `father`, `family`, `dob`, `gender`, `building`, `street`, `zone`, `nationality`, `idcardNo`, `mobile`, `fatherMobile`, `email`, `fatherEmail`, `school`, `classGrade`, `hearAbout`, `referalName`, `referalMobile`, `other`, `language`, `courseDetails`, `photo`, `studColor`) VALUES
(1, 'Jawahara', 1000, '', '', '', 'female', '', '', '', '', '', '55557670', '', '', '', '', '6,2', '', '', '', '', '', '', '', '#cea017'),
(2, 'Rowda', 1001, '', '', '', 'female', '', '', '', '', '', '55557670', '', '', '', '', '2', '', '', '', '', '', '', '', '#71a64b'),
(3, 'Rabab Mohmed', 1002, '', '', '', 'female', '', '', '', '', '', '87687875875', '', '', '', '', '', '', '', '', '', '', '', '', '#8cc8d8');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance`
--

CREATE TABLE `student_attendance` (
  `ID` int(11) NOT NULL,
  `scheduleId` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_group`
--

CREATE TABLE `student_group` (
  `ID` int(11) NOT NULL,
  `groupName` varchar(50) NOT NULL,
  `subjectId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_group`
--

INSERT INTO `student_group` (`ID`, `groupName`, `subjectId`) VALUES
(2, 'English', 9);

-- --------------------------------------------------------

--
-- Table structure for table `student_schedule`
--

CREATE TABLE `student_schedule` (
  `ID` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `teacherId` int(11) NOT NULL,
  `classDate` date NOT NULL,
  `timeFrom` time NOT NULL,
  `timeTo` time NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_schedule`
--

INSERT INTO `student_schedule` (`ID`, `studentId`, `subjectId`, `teacherId`, `classDate`, `timeFrom`, `timeTo`, `amount`) VALUES
(11, 1, 9, 12, '2016-03-05', '10:00:00', '11:00:00', 150);

-- --------------------------------------------------------

--
-- Table structure for table `student_subject`
--

CREATE TABLE `student_subject` (
  `ID` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_subject`
--

INSERT INTO `student_subject` (`ID`, `studentId`, `subjectId`) VALUES
(2, 1, 9),
(4, 2, 9),
(5, 1, 48),
(6, 3, 22);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `ID` int(11) NOT NULL,
  `subjectName` varchar(30) NOT NULL,
  `place` varchar(20) NOT NULL,
  `countType` varchar(20) NOT NULL,
  `fee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`ID`, `subjectName`, `place`, `countType`, `fee`) VALUES
(1, 'English', 'Center', 'Group', 100),
(2, 'Maths', 'Home', 'Group', 200),
(3, 'Arabic', 'Center', 'Group', 100),
(6, 'Maths', 'Home', 'Individual', 250),
(7, 'Maths', 'Center', 'Group', 100),
(8, 'Maths', 'Center', 'Individual', 150),
(9, 'English', 'Center', 'Individual', 150),
(10, 'Arabic', 'Center', 'Individual', 150),
(11, 'Arabic', 'Home', 'Individual', 250),
(12, 'Arabic', 'Home', 'Group', 200),
(13, 'English', 'Home', 'Group', 200),
(14, 'English', 'Home', 'Individual', 250),
(15, 'Science', 'Center', 'Group', 100),
(16, 'Science', 'Center', 'Individual', 150),
(17, 'Science', 'Home', 'Group', 200),
(18, 'Science', 'Home', 'Individual', 250),
(19, 'Act/sat', 'Center', 'Group', 100),
(20, 'Act/sat', 'Center', 'Individual', 150),
(21, 'Act/sat', 'Home', 'Group', 200),
(22, 'Act/sat', 'Home', 'Individual', 250),
(27, 'Ielts/toefl', 'Center', 'Group', 100),
(28, 'Ielts/toefl', 'Center', 'Individual', 150),
(29, 'Ielts/toefl', 'Home', 'Group', 200),
(30, 'Ielts/toefl', 'Home', 'Individual', 250),
(31, 'Business Studies', 'Center', 'Group', 100),
(32, 'Business Studies', 'Center', 'Individual', 150),
(33, 'Business Studies', 'Home', 'Group', 200),
(34, 'Business Studies', 'Home', 'Individual', 250),
(35, 'Travel & Tourism', 'Center', 'Group', 100),
(36, 'Travel & Tourism', 'Center', 'Individual', 150),
(37, 'Travel & Tourism', 'Home', 'Group', 200),
(38, 'Travel & Tourism', 'Home', 'Individual', 250),
(39, 'Physics', 'Center', 'Group', 100),
(40, 'Physics', 'Center', 'Individual', 150),
(41, 'Physics', 'Home', 'Group', 200),
(42, 'Physics', 'Home', 'Individual', 250),
(43, 'Chemistry', 'Center', 'Group', 100),
(44, 'Chemistry', 'Center', 'Individual', 150),
(45, 'Chemistry', 'Home', 'Group', 200),
(46, 'Chemistry', 'Home', 'Individual', 250),
(47, 'Biology', 'Center', 'Group', 100),
(48, 'Biology', 'Center', 'Individual', 150),
(49, 'Biology', 'Home', 'Group', 200),
(50, 'Biology', 'Home', 'Individual', 250),
(51, 'Quran', 'Center', 'Group', 100),
(52, 'Quran', 'Center', 'Individual', 150),
(53, 'Quran', 'Home', 'Group', 200),
(54, 'Quran', 'Home', 'Individual', 250),
(59, 'All Subjects', 'Center', 'Group', 100),
(60, 'All Subjects', 'Center', 'Individual', 150),
(61, 'All Subjects', 'Home', 'Group', 200),
(62, 'All Subjects', 'Home', 'Individual', 250);

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

CREATE TABLE `vacancy` (
  `ID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `qualification` varchar(200) NOT NULL,
  `experience` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy`
--

INSERT INTO `vacancy` (`ID`, `title`, `description`, `qualification`, `experience`) VALUES
(1, 'Female Teacher English', 'Female Teacher qualified in English as main and can handle Science subjects for smaller grades/levels.', 'Post Graduation in subjects (English),\r\nGood knowledge English.', '2 + years of teaching experience.'),
(2, 'Female Teacher Science', 'Female Teacher qualified in Science as main subject and can handle Maths subjects also.', 'Post Graduation in subjects (Science),\r\nGood knowledge Science subjects.', '3 + years of teaching experience.'),
(3, 'Female Teacher Maths', 'Female Teacher qualified in Maths as main subject and can handle Physics and Chemistry subjects also.Post Graduation in subjects (Maths)\r\n\r\nGood knowledge Maths subjects.', 'Post Graduation in subjects (English),\r\n\r\nGood knowledge Maths English.', '4 + years of teaching experience.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `subjectId` (`subjectId`);

--
-- Indexes for table `feepayment`
--
ALTER TABLE `feepayment`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `studentId` (`studentId`);

--
-- Indexes for table `group_details`
--
ALTER TABLE `group_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `designation` (`designation`),
  ADD KEY `designation_2` (`designation`),
  ADD KEY `designation_3` (`designation`);

--
-- Indexes for table `staff_subject`
--
ALTER TABLE `staff_subject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `staffId` (`staffId`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `scheduleId` (`scheduleId`);

--
-- Indexes for table `student_group`
--
ALTER TABLE `student_group`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `student_schedule`
--
ALTER TABLE `student_schedule`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `studentId` (`studentId`),
  ADD KEY `subjectId` (`subjectId`),
  ADD KEY `teacherId` (`teacherId`);

--
-- Indexes for table `student_subject`
--
ALTER TABLE `student_subject`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `studentId` (`studentId`),
  ADD KEY `subjectId` (`subjectId`),
  ADD KEY `studentId_2` (`studentId`),
  ADD KEY `subjectId_2` (`subjectId`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vacancy`
--
ALTER TABLE `vacancy`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `feepayment`
--
ALTER TABLE `feepayment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `group_details`
--
ALTER TABLE `group_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `staff_subject`
--
ALTER TABLE `staff_subject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `student_attendance`
--
ALTER TABLE `student_attendance`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_group`
--
ALTER TABLE `student_group`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student_schedule`
--
ALTER TABLE `student_schedule`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `student_subject`
--
ALTER TABLE `student_subject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `vacancy`
--
ALTER TABLE `vacancy`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD CONSTRAINT `enquiry_ibfk_1` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `feepayment`
--
ALTER TABLE `feepayment`
  ADD CONSTRAINT `feepayment_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `staff_subject`
--
ALTER TABLE `staff_subject`
  ADD CONSTRAINT `staff_subject_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_attendance`
--
ALTER TABLE `student_attendance`
  ADD CONSTRAINT `student_attendance_ibfk_1` FOREIGN KEY (`scheduleId`) REFERENCES `student_schedule` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_schedule`
--
ALTER TABLE `student_schedule`
  ADD CONSTRAINT `student_schedule_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_schedule_ibfk_3` FOREIGN KEY (`teacherId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_schedule_ibfk_4` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student_subject`
--
ALTER TABLE `student_subject`
  ADD CONSTRAINT `student_subject_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `student` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_subject_ibfk_2` FOREIGN KEY (`subjectId`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
